var game = new Phaser.Game(700, 900, Phaser.CANVAS, 'test', null, false, false);
var BasicGame = function (game) { };
BasicGame.Boot = function (game) { };

//=========================== DECLARATIONS ====================================

// game tuning numbers
var recoveryLineBonus = 250;
var fuelDecrease = 0.25;
var usaMissileDelaySize = 60;
var ussrMissileDelaySize = 120;
var usaMissileDamage = 300;
var ussrMissileDamage = 150;
var missileHitArray = [true, false];
var missileSpeedArray = [0.5, 0, 0.5, 1];
var hudFloatDirection = [30, 20, 10, -10, -20, -30]
var shieldRefreshRate = 5;
var fuelRefreshRate = 5;
var ammoRefreshRate = 5;
var missileDelayUnitModifier = 1;
var ussrWaveDelay = 0;
var ussrSubWaveDelay = 0;
var usaHealth = 1500;
var usaFullHealth = 1500;
var hudCounter = 1000;
var hudCounterThreshold = 2400;

var waveCounterDelay = 0;
var waveCounterDelayThreshold = 100;

var firstLoopRun = true;
var levelCompleted = true;
//timing
var tweenMultiplier = 1;
var tweenInverseMultiplier = 1;
var slowMotionMultiplier = 1;

var cursorPosCorrection = 112;
var catapultCoords = [3, 1]; //position of catapult on deck
var catapultTrackCoords = [3, 0];//position of catapult track on deck
var fuelCoords = [1, 0]; //position of fuel on deck
var shieldCoords = [1, 2]; //position of shield on deck
var ammoCoords = [1, 4]; //position of ammo on deck
var liftCoords = [3, 7];

var deckOffsetX = 15;   //this is where deck X starts in units of isoBaseSize
var deckOffsetY = 12;   //this is where deck Y starts in units of isoBaseSize
var deckWidth = 4;
var deckHeight = 8;
var resetting = false;
var ussrWaveUpdate = false;

var isoBaseSize = 32;        // size of isometric sprites
var isoGameSize = 64;        // gameboard size (ie.e 2 * 2 sprites of deckspace)
var deckSpriteIsoZ = 80;     // Z is set to keep decksprites above the deck level
var isoGroup;                // gropup for all isometric sprites
var graphicsStatusGroup;
var usaProfileGroup;
var ussrProfileGroup;
var textGroup;
var ffwdGroup;
var cursorPos;

var currentPositionIndicator; // green square
var currentPositionIndicatorBox; // blue wireframe box
var newPositionIndicatorBox; //  green wireframe box

var infoPanelAmmoText;
var infoPanelShieldText;
var infoPanelFuelText;
var infoPanelTextFireRate;
var infoPanelText2;
var infoPanelText3;

var infoPanelLevelText1;
var infoPanelLevelText2;
var infoPanelLevelText3;
var infoPanelLevelText4;
var infoPanelLevelText5;
var infoPanelLevelText6;

var spriteButtons = [];
var moveIndicators = [];
var chosenPath = [];
var hawkEyeChevrons = [];
var seakingJammers = [];
var foxbatIntelTexts = [];
var foxbatIntelMorseCodes = [];
var foxbatIntel = false;

var landingSequenceRunning = false;

var ussrProfiles = [];
var usaProfiles = [];

var usaMissiles = [];
var ussrMissiles = [];

var ussrWave = 0; //starts at zero index
var ussrSubwave = -1; //first loop starts us at zero index
var ussrWaveFullHealth = 0;
var ussrWaveHealth = 0;
//text
var recoveryAmmoText;
var recoveryShieldText;
var recoveryFuelText;
var recoveryBolterText;
var mainTitleLine1Text;
var mainTitleLine2Text;
var mainTitleLine3Text;
var infoPanelTitleText;

var foxbatIntelSlot1;
var foxbatIntelSlot2;
var foxbatIntelSlot3;
var foxbatIntelSlot4;

var graphicsProfile;
var graphicsStatusBars;

var admiral;
var hud;
var hudSprite;

//== 5 sliding background cloud sprites
var cloud_1;
var cloud_2;
var cloud_3;
var cloud_4;
var cloud_5;
var deck_1;
var cold_war;
var burger_menu;
var pause_menu;
var ffwd_1;
var ffwd_2;
var ffwd_3;

var mouthAnimation;
var fireAnimation;
var fireAnimation;

var flagAnimation

var usaHealthBar;
var ussrHealthBar;
var usaHealthBarDelta;
var ussrHealthBarDelta;
var ussrHealthDelta = 0;
var usaHealthDelta = 0;

var spriteInfoPanelProfile;
var spriteInsigniaPanel;
var spriteInfoPanelBackground;
var spriteInfoPanelGuid = null; //for usa info panel
var spriteInfoPanelSlot = null; // for ussr info panel
var missileChevronStatusBar;

var usaAlive = true;

var getReadyNextWaveText = null;

var spriteActionArray = [];
//deck
spriteActionArray[0] = 'sit_on_deck';
spriteActionArray[1] = 'move_on_deck';
spriteActionArray[1] = 'busy_moving_on_deck';

//hover
spriteActionArray[2] = 'hover_up';
spriteActionArray[3] = 'hover_level';
spriteActionArray[4] = 'hover_down';
spriteActionArray[5] = 'hover_leave';
spriteActionArray[6] = 'hover_return';
spriteActionArray[7] = 'ready_for_takeoff';
spriteActionArray[8] = 'takeoff';
spriteActionArray[9] = 'landing';
spriteActionArray[10] = 'away_from_carrier';
spriteActionArray[11] = 'in_combat';
spriteActionArray[12] = 'return_to_carrier';
spriteActionArray[13] = 'dead';
spriteActionArray[14] = 'crashing';
spriteActionArray[15] = 'crashed';

var spriteTypeArray = [];
spriteTypeArray[0] = 'harrier';
spriteTypeArray[1] = 'tomcat';
spriteTypeArray[2] = 'hawkeye';
spriteTypeArray[3] = 'seaking';
spriteTypeArray[4] = 'warthog';
spriteTypeArray[5] = 'foxbat_defect';
spriteTypeArray[6] = 'huey';

var directionArray = [];
directionArray[0] = "_n";
directionArray[1] = "_e";
directionArray[2] = "_s";
directionArray[3] = "_w";

var sprite_deck = [];
// type, direction, x, y
var usaWaves = [
        [
           [0, 1, 0, 1]
        ],
        [
           [0, 1, 0, 1]
        ],
        [
            [0, 1, 0, 1],
            [0, 1, 0, 2]
        ],
        [
            [1, 0, 3, 2],
            [3, 1, 0, 0]
        ],
        [
            [0, 1, 0, 1],
            [0, 1, 0, 2],
            [2, 0, 3, 2]
        ],
        [
            [0, 1, 0, 0],
            [0, 1, 0, 1],
            [0, 1, 0, 2],
            [0, 1, 0, 3],
            [0, 1, 0, 4],
            [0, 1, 0, 5],
            [0, 1, 0, 6],
            [0, 1, 0, 7]
        ],
        [
            [1, 0, 3, 0],
            [1, 0, 3, 2],
            [1, 0, 1, 5],
            [1, 0, 1, 6],
            [1, 0, 1, 7],
        ],
        [
            [3, 1, 0, 0],
            [4, 0, 3, 2]
        ],
        [
            [2, 0, 3, 2],
            [2, 0, 3, 3],
            [2, 0, 3, 4],
            [1, 0, 3, 5]
        ],
        [
            [3, 1, 0, 0],
            [3, 1, 0, 1],
        ],
        [
             [4, 0, 3, 2],
             [4, 0, 3, 3],
             [4, 0, 3, 4],
             [4, 0, 3, 5],
             [4, 0, 3, 6],
             [4, 0, 3, 7],
        ],
        [
             [3, 1, 0, 0],
             [3, 1, 0, 1],
             [1, 3, 1, 1],
             [2, 1, 2, 2],
             [0, 2, 2, 4],
             [4, 3, 3, 7],
             [0, 3, 3, 0],
             [0, 3, 0, 7]
        ],
        [
            [6, 1, 0, 0],
            [6, 0, 0, 1],
            [6, 2, 0, 2],
            [6, 3, 0, 3],
            [6, 2, 0, 4],
            [6, 3, 0, 5],
            [6, 2, 0, 6],
            [6, 0, 0, 7],
            [6, 0, 1, 0],
            [1, 3, 1, 1],
            [6, 2, 1, 2],
            [6, 1, 1, 3],
            [6, 1, 1, 4],
            [6, 0, 1, 5],
            [6, 1, 1, 6],
            [6, 3, 1, 7],
            [6, 1, 2, 0],
            [6, 1, 2, 1],
            [6, 0, 2, 2],
            [6, 3, 2, 3],
            [6, 2, 2, 4],
            [6, 2, 2, 5],
            [6, 3, 2, 6],
            [6, 0, 2, 7],
            [6, 2, 3, 0],
            [6, 0, 3, 1],
            [6, 3, 3, 2],
            [6, 2, 3, 3],
            [6, 2, 3, 4],
            [1, 1, 3, 5],
            [6, 0, 3, 6],
            [6, 3, 3, 7]
        ],
        [
            [0, 1, 0, 1]
        ],
        [
            [0, 1, 0, 1]
        ],
        [
            [3, 1, 0, 0],
            [3, 1, 0, 1],
            [0, 1, 0, 2],
            [0, 1, 0, 3],
            [0, 1, 0, 4],
            [0, 1, 0, 5],
            [0, 1, 0, 6],
            [0, 1, 0, 7],
            [1, 3, 3, 2],
            [1, 3, 3, 3],
            [4, 3, 3, 4],
            [4, 3, 3, 5],
            [2, 3, 3, 6],
            [2, 3, 3, 7],
        ]
];

var ussrWaves = [
    [
        [
            null,
            null,
            null,
            'mig15'
        ]
    ],
    [
        [
            null,
            null,
            null,
            'mig15'
        ]
    ],
    [
        [
            null,
            null,
            'mig21',
            null
        ]
    ],
    [
        [
            null,
            null,
            'mig15',
            'mig21'
        ]
    ],
    [
        [
            null,
            'mig15',
            null,
            'mig15'
        ]
    ],
    [
        [
            'mig15',
            'mig15',
            'mig15',
            'mig15',
        ],
        [
            null,
            null,
            null,
            'bear'
        ]
    ],
    [
        [null,
         null,
         null,
         'fulcrum'
        ]
    ],
    [
        [
            'foxbat',
            null,
            null,
            'mig15'
        ],
        [
            null,
            null,
            'fulcrum',
            'fulcrum'
        ]
    ],
    [
        [
            'bear',
            null,
            null,
            null
        ]
    ],
    [
        [
            null,
            null,
            null,
            'foxbat'
        ],
        [
            null,
            null,
            'foxbat',
            'foxbat'
        ]
    ],
    [
        [
            null,
            'fulcrum',
            'fulcrum',
            'fulcrum'
        ],
        [
            null,
            'fulcrum',
            'fulcrum',
            'fulcrum'
        ],
        [
            null,
            'fulcrum',
            'fulcrum',
            'fulcrum'
        ]
    ],
    [
        [
            null,
            null,
            null,
            'mig15'
        ],
        [
            'mig21',
            'bear',
            null,
            'fulcrum',
        ],
        [
            null,
            null,
            null,
            'mig21'
        ],
        [
            'mig21',
            'fulcrum',
            'bear',
            'mig21'
        ]
    ],
    [
        [
            'mig15',
            'mig15',
            'mig15',
            'mig15'
        ],
        [
            null,
            'mig15',
            'mig15',
            'mig15'
        ]
    ],
     [
        [
            null,
            null,
            null,
            'mig15'
        ]
     ],
     [
        [
            null,
            null,
            null,
            'mig15'
        ]
     ],
    [
        [
            'mig15',
            null,
            null,
            null
        ],
        [
            null,
            'mig21',
            null,
            null
        ],
        [
            null,
            null,
            'fulcrum',
            null
        ],
        [
            null,
            null,
            null,
            'bear'
        ],
        [
            null,
            'mig15',
            null,
            'mig15'
        ],
        [
            'mig21',
            null,
            'mig21',
            null
        ],
        [
            null,
            null,
            null,
            'mig15'
        ],
        [
            null,
            null,
            'foxbat',
            'mig15'
        ],
        [
            null,
            'fulcrum',
            'fulcrum',
            'fulcrum'
        ],
        [
            null,
            'bear',
            null,
            'bear'
        ],
        [
            'bear',
            'foxbat',
            'fulcrum',
            'mig21'
        ]
    ],
    //this level needed to reset level 16 correctly
    [
        [null, null, null, null]
    ]
];

var levelText = [
    [
        ['Hostile bandit attacking!'],
        [' - Tap HARRIER to select'],
        [' - Tap GREEN SQUARE to move'],
        [' - Tap ARROW to take-off'],
        [' - Tap HARRIER to return'],
    ],
    [
        [' Your HARRIER is depleted.'],
        [' Before engaging recharge:'],
        [' - RED ammo'],
        [' - YELLOW shield'],
        [' - PURPLE fuel'],
    ],
    [
        ['Bogey detected at higher'],
        ['altitude. Stack 2 HARRIERS'],
        ['to make contact. DO NOT'],
        ['exceed your BINGO, return'],
        ['to mother and reprovision'],
    ],
    [
        ['Use CAT to launch TOMCAT'],
        ['Use SEAKING to jam that'],
        ['threat ASAP! Maintain a'],
        ['high-level of situation'],
        ['awareness at all times!'],
    ],
    [
        ['The HAWKEYE can facilitate'],
        ['a strategic air-superiorty'],
        ['outcome. It\'s RADAR will'],
        ['improve the fire-rate of'],
        ['your combat assets. Hooyah!'],
    ],
    [
        ['Reports of multiple'],
        ['hostiles incoming. Scramble'],
        ['the whole Air-Wing!'],
        ['Be advised we suspect they'],
        ['may be escorting a BOMBER!'],
    ],
    [
        ['F14\'s cost taxpayers US$38M'],
        ['per unit.Prang it and it\'ll'],
        ['come out your pay Ace! Keep'],
        ['CAT TRACK and RUNWAY clear'],
        ['of aircraft, or else.....']
    ],
    [
        ['Intel indicates a FOXBAT'],
        ['will attempt to defect if'],
        ['it can breakaway from it\'s'],
        ['squadron. Clear it\'s path!'],
        ['Expect retaliation!']
    ],
    [
        ['A BOMBER is approacing at'],
        ['high-altitude. Intercept.'],
        ['SIERRA-HOTEL flying may '],
        ['prevent this turning into'],
        ['another CHARLIE-FOXTROT!'],
    ],
    [
        ['More FOXBATs approaching.'],
        ['Exfiltrate as many as'],
        ['possible. Use Electronic'],
        ['Counter Measures to protect'],
        ['the boat. Chocks away!']
    ],
    [
        ['A full squadron of FULCRUMs'],
        ['is on the horizon. Use our'],
        ['massive superior firepower'],
        ['to gain tactical advantage'],
        ['in the operational theatre.']
    ],
    [
        ['SEAKINGS can buy you time'],
        ['whilst threat level is low.'],
        ['Tidy up the flight-deck and'],
        ['replenish assets resources'],
        ['before you proceed.']
    ],
    [
        ['Saigon has fallen! HUEYs'],
        ['on evac are compromizing'],
        ['flight-deck operations'],
        ['Ditch them ASAP and'],
        ['don\'t let them return!']
    ],
     [
        ['todo: add level'],
        [''],
        [''],
        [''],
        ['']
     ],
     [
        ['todo: add level'],
        [''],
        [''],
        [''],
        ['']
     ],
     [
        ['The Reds are mounting a'],
        ['major offensive. Protect'],
        ['your healthbar and don\'t'],
        ['let this go FUBAR!'],
        ['Good luck Ace!']
     ],
     [
        ['Congratulations TOPGUN!'],
        ['That was some real Ace'],
        ['flying son! You\'ve saved'],
        ['the free world from the'],
        ['Commies. Outstanding!']
     ],
];

var water = [];

//=====================  GAME ===============================================
BasicGame.Boot.prototype =
{
    preload: function () {
        game.time.advancedTiming = true;
        game.debug.renderShadow = false;
        game.stage.disableVisibilityChange = true;

        game.plugins.add(new Phaser.Plugin.Isometric(game));

        game.load.atlasJSONHash('tileset', 'assets/tileset.png', 'assets/tileset.js');
        game.load.atlasJSONHash('insigniaset', 'assets/insignia.png', 'assets/insignia.js');
        game.load.image('admiral', 'assets/admiral.png');
        game.load.image('hud', 'assets/hud.png');
        game.load.image('cloud_1', 'assets/cloud_1.png');
        game.load.image('cloud_2', 'assets/cloud_2.png');
        game.load.image('cloud_3', 'assets/cloud_3.png');
        game.load.image('cloud_4', 'assets/cloud_4.png');
        game.load.image('cloud_5', 'assets/cloud_5.png');
        game.load.image('deck_1', 'assets/deck2.png');
        game.load.image('cold_war', 'assets/cold_war.png');
        game.load.image('pause_menu', 'assets/pause_menu.png');
        game.load.spritesheet('fireSprites', 'assets/fire.png', 64, 64, 6);
        game.load.spritesheet('flagSprites', 'assets/flag.png', 84, 92, 2);
        game.load.spritesheet('mouthSprites', 'assets/admiral_mouth.png', 16, 14, 6);
        game.physics.startSystem(Phaser.Plugin.Isometric.ISOARCADE);
        game.iso.anchor.setTo(0.528, 0.14);

        //=== fonts
        game.load.bitmapFont('fontYellow', 'assets/font/font_yellow.png', 'assets/font/font.xml');
        game.load.bitmapFont('fontRed', 'assets/font/font_red.png', 'assets/font/font.xml');
        game.load.bitmapFont('fontPurple', 'assets/font/font_purple.png', 'assets/font/font.xml');
        game.load.bitmapFont('fontBlue', 'assets/font/font_blue.png', 'assets/font/font.xml');
    },
    create: function () {
        game.time.slowMotion = slowMotionMultiplier;

        this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        this.scale.minWidth = 320;
        this.scale.minHeight = 427;
        this.scale.maxWidth = 720;
        this.scale.maxHeight = 960;
        this.scale.refresh();
        this.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;

        admiral = game.add.sprite(130, 685, 'admiral');
        admiral.visible = false;
        admiral.scale.setTo(2, 2);

        cloud_1 = game.add.sprite(-50, 0, 'cloud_1');
        cloud_2 = game.add.sprite(-50, 100, 'cloud_2');
        cloud_3 = game.add.sprite(-50, 200, 'cloud_3');
        cloud_4 = game.add.sprite(-50, 300, 'cloud_4');
        cloud_5 = game.add.sprite(-50, 391, 'cloud_5');
        deck_1 = game.add.sprite(-3, 443, 'deck_1');
        cold_war = game.add.sprite(100, 220, 'cold_war');

        SetupFfwd();

        cold_war.scale.setTo(1.5, 1.5)
        cold_war.visible = false;
        cloud_1.scale.setTo(4.5, 4);
        cloud_2.scale.setTo(4.5, 4);
        cloud_3.scale.setTo(4.5, 4);
        cloud_4.scale.setTo(4.5, 4);
        cloud_5.scale.setTo(4, 4);

        DrawHealthBars();
        cloud_1.sendToBack();
        cloud_2.sendToBack();
        cloud_3.sendToBack();
        cloud_4.sendToBack();

        mouthAnimation = game.add.sprite(246, 791, 'mouthSprites');
        mouthAnimation.animations.add('mouths');
        mouthAnimation.scale.setTo(2, 2);
        mouthAnimation.visible = false;

        fireAnimation = game.add.sprite(-300, -300, 'fireSprites');
        fireAnimation.animations.add('flames');
        fireAnimation.x = 0;
        fireAnimation.y = 0;
        fireAnimation.visible = false;

        flagAnimation = game.add.sprite(60, 565, 'flagSprites');
        flagAnimation.animations.add('flags');
        flagAnimation.animations.play('flags', 0.5, true);
        flagAnimation.bringToTop();
        flagAnimation.scale.setTo(0.5, 0.5)

        spriteInsigniaPanel = SetFlatSprite(170, 400, 100, 'insigniaset', 'insignia_11', usaProfileGroup);
        spriteInsigniaPanel.scale.setTo(4, 4);
        spriteInsigniaPanel.visible = false;

        spriteInfoPanelProfile = SetFlatSprite(350, 760, 101, 'tileset', 'nothing', usaProfileGroup);
        spriteInfoPanelBackground = SetFlatSprite(380, 710, 100, 'tileset', 'info_panel_background', usaProfileGroup);
        spriteInfoPanelBackground.scale.setTo(10, 6);
        missileChevronStatusBar = SetFlatSprite(280, 825, 101, 'tileset', 'nothing', usaProfileGroup);

        isoGroup = game.add.group();
        graphicsStatusGroup = game.add.group();
        textGroup = game.add.group();

        infoPanelTextFireRate = game.make.text(220, 825, '', { font: "18px Courier", fill: "#0xFFFFFF" });
        infoPanelTextFireRate.fontWeight = 'bold';
        infoPanelText2 = game.make.text(400, 845, '', { font: "18px Courier", fill: "#0xFFFFFF" });
        infoPanelText3 = game.make.text(400, 865, '', { font: "18px Courier", fill: "#0xFFFFFF" });



        infoPanelLevelText1 = game.make.text(395, 725, '', { font: "18px Courier", fill: "#0xFFFFFF" });
        infoPanelLevelText2 = game.make.text(395, 750, '', { font: "18px Courier", fill: "#0xFFFFFF" });
        infoPanelLevelText3 = game.make.text(395, 775, '', { font: "18px Courier", fill: "#0xFFFFFF" });
        infoPanelLevelText4 = game.make.text(395, 800, '', { font: "18px Courier", fill: "#0xFFFFFF" });
        infoPanelLevelText5 = game.make.text(395, 825, '', { font: "18px Courier", fill: "#0xFFFFFF" });
        infoPanelLevelText6 = game.make.text(395, 850, '', { font: "18px Courier", fill: "#0xFFFFFF" });

        infoPanelAmmoText = game.make.text(400, 770, '', { font: "12px Courier", fill: "#0xFFFFFF" });
        infoPanelShieldText = game.make.text(400, 780, '', { font: "12px Courier", fill: "#0xFFFFFF" });
        infoPanelFuelText = game.make.text(400, 790, '', { font: "12px Courier", fill: "#0xFFFFFF" });

        textGroup.add(infoPanelTextFireRate);
        textGroup.add(infoPanelText2);
        textGroup.add(infoPanelText3);
        textGroup.add(infoPanelLevelText1);
        textGroup.add(infoPanelLevelText2);
        textGroup.add(infoPanelLevelText3);
        textGroup.add(infoPanelLevelText4);
        textGroup.add(infoPanelLevelText5);
        textGroup.add(infoPanelLevelText6);

        textGroup.add(infoPanelAmmoText);
        textGroup.add(infoPanelShieldText);
        textGroup.add(infoPanelFuelText);

        PopulateDeckSprites(ussrWave);
        DrawDeckSprites();
        SetupDeckSprites();
        DrawRecoveryText();
        DrawMainTitle();
        DrawFoxbatIntel();

        game.time.events.add(Phaser.Timer.SECOND * 4, function () {
            DrawWaveText('     alert', '    status', '   defcon 1');
            burger_menu.visible = true;
        });

        graphicsProfile = game.add.graphics(0, 0);
        graphicsStatusBars = game.add.graphics(0, 0);
        graphicsStatusGroup.add(graphicsStatusBars);

        game.iso.simpleSort(isoGroup);

        cursorPos = new Phaser.Plugin.Isometric.Point3(); //Provide a 3D position for the cursor
        SetupBurgerMenu();

        hudSprite = game.add.sprite(100, 0, 'hud');
        hudSprite.visible = false;
        hudSprite.scale.setTo(1.5, 1.5);
        hudSprite.bringToTop();
        MusicOn();

        DrawSea();
    },
    update: function () {
        if (graphicsProfile)
            graphicsProfile.clear();
        if (graphicsStatusBars)
            graphicsStatusBars.clear();
        UpdateUssrWave();
        UpdateProfileStackUsa();
        UpdateMissileUsa();
        UpdateMissileUssr();
        AnimateProfile(ussrProfiles, 'ussr');
        AnimateProfile(usaProfiles, 'usa');
        UpdateUsaHealthBar(usaHealthDelta);
        UpdateUssrHealthBar(ussrHealthDelta);
        RunMainLoop();
        DrawSpriteInfoPanel();
        game.world.bringToTop(graphicsStatusGroup);
        game.world.bringToTop(textGroup);
        game.iso.simpleSort(isoGroup);
        game.iso.unproject(game.input.activePointer.position, cursorPos);
        DrawHud();
        AnimateWater();
    },
    render: function () {
        game.debug.text(game.time.fps || '--', 350, 20, "#000000");
    }
};

game.state.add('Boot', BasicGame.Boot);
game.state.start('Boot');

//=======================================================================================

function UpdateUssrWave() {

    var updateSubWave = false;
    var updateWave = false;

    if (!ussrWaveUpdate && usaAlive && !IsUssrProfileStillExist()) {
        if (isUpdateUssrSubWave()) {
            ussrSubwave++;
            updateSubWave = true;
        }
        else if (isUpdateUssrWave() || resetting) {
            waveCounterDelay += 1;

            if (waveCounterDelay == 1)
                DestroyAll();

            if (waveCounterDelay >= waveCounterDelayThreshold * tweenMultiplier || resetting) {

                DrawWaveText('     alert', '    status', '   DEFCON ' + (ussrWave + 1));

                if (!resetting)
                    ussrWave++;

                usaHealth = usaFullHealth;
                ussrSubwave = 0;
                DestroyAll();
                DestroyAllSelections();
                PopulateDeckSprites(ussrWave);
                DrawDeckSprites();
                SetupDeckSprites();
                updateWave = true;
            }
        }

        if (resetting || updateSubWave || updateWave && (waveCounterDelay >= waveCounterDelayThreshold * tweenMultiplier)) {

            ussrWaveUpdate = true;
            if (ussrWaveHealth <= 0) {
                ussrWaveFullHealth = ussrWaveHealth = GetUssrWaveHealth(ussrWave);
                DrawHealthBarsAnimateUp();
                DrawLevelTextInfoPanel(ussrWave);

                if (levelCompleted) {

                    if (ussrWave > 0) {
                        DrawInsigniaPanel(ussrWave);
                        game.paused = true;
                        game.input.onDown.add(Unpause, self);
                    }
                }
                levelCompleted = true;
            }
            waveCounterDelay = 0;
        }
    }

    if (ussrWaveUpdate) {
        UpdateUssrSubWave(ussrWave, ussrSubwave);
        ussrWaveUpdate = false;
    }
}

function Unpause(event) {
    if (game.paused) {
        var selectedLevel = GetSelectedLevel(event.x, event.y);
        if (selectedLevel == -2)
            MusicOn();

        if (selectedLevel == -3)
            MusicOff();

        if (selectedLevel > -1) {
            usaHealth = usaFullHealth;
            ussrWave = selectedLevel;
            ussrWaveFullHealth = ussrWaveHealth = GetUssrWaveHealth(ussrWave);
            usaAlive = true;
            resetting = true;
            ussrSubwave = 0;
            ussrWaveHealth = 0;
            ussrWaveUpdate = false;
            DestroyAll();
            DestroyAllSelections();
        }
    }
    game.paused = false;
    spriteInsigniaPanel.visible = false;
    mainTitleLine1Text.visible = false;
    mainTitleLine2Text.visible = false;
    mainTitleLine3Text.visible = false;
    if (pause_menu)
        pause_menu.destroy();

    textGroup.visible = true;
}
function ResetFfwd(number) {
    ffwd_1.loadTexture('tileset', (number == 1) ? 'ffwd_1_on' : 'ffwd_1_off');
    ffwd_2.loadTexture('tileset', (number == 2) ? 'ffwd_2_on' : 'ffwd_2_off');
    ffwd_3.loadTexture('tileset', (number == 3) ? 'ffwd_3_on' : 'ffwd_3_off');
}

function ResetFlagAnimation() {
    flagAnimation.animations.stop();
    flagAnimation.animations.play('flags', 0.5 * tweenInverseMultiplier, true);
}

function SetupFfwd() {
    ffwd_1 = SetFlatSprite(110, 518, 0, 'tileset', 'ffwd_1_on', ffwdGroup);
    ffwd_1.inputEnabled = true;
    ffwd_1.input.useHandCursor = true;
    ffwd_1.events.onInputDown.add(function () {
        tweenMultiplier = 1;
        tweenInverseMultiplier = 1;
        ResetFfwd(1);
        ResetFlagAnimation();
    });

    ffwd_2 = SetFlatSprite(168, 484, 0, 'tileset', 'ffwd_2_off', ffwdGroup);
    ffwd_2.inputEnabled = true;
    ffwd_2.input.useHandCursor = true;
    ffwd_2.events.onInputDown.add(function () {
        tweenMultiplier = 0.5;
        tweenInverseMultiplier = 2;
        ResetFfwd(2);
        ResetFlagAnimation();
    });

    ffwd_3 = SetFlatSprite(222, 450, 0, 'tileset', 'ffwd_3_off', ffwdGroup);
    ffwd_3.inputEnabled = true;
    ffwd_3.input.useHandCursor = true;
    ffwd_3.events.onInputDown.add(function () {
        tweenMultiplier = 0.33;
        tweenInverseMultiplier = 3;
        ResetFfwd(3);
        ResetFlagAnimation();
    });
}

function SetupBurgerMenu() {
    burger_menu = game.add.sprite(0, 787, 'tileset', 'burger');
    burger_menu.visible = false;
    burger_menu.inputEnabled = true;
    burger_menu.input.useHandCursor = true;
    burger_menu.bringToTop();
    burger_menu.events.onInputDown.add(function () {
        game.paused = true;
        game.input.onDown.add(Unpause, self);
        pause_menu = game.add.sprite(170, 30, 'pause_menu');
        pause_menu.visible = true;
        pause_menu.bringToTop();
        levelCompleted = false;
        DrawMenuSelection(ussrWave);
    });
}

function PopulateDeckSprites(wave) {
    DestroySpriteDeck();
    sprite_deck = [];
    //(x 0-3) / y (0-8)
    // type, direction, x, y, destinationX, destinationY, shield, fuel, ammo, action, sprite, counter, guid, fightSlot

    if (usaWaves[wave]) {
        for (var i = 0; i < usaWaves[wave].length; i++) {

            if (usaWaves[wave][i]) {
                var aircraft;
                var type = usaWaves[wave][i][0];
                var direction = usaWaves[wave][i][1];
                var x = usaWaves[wave][i][2];
                var y = usaWaves[wave][i][3];

                //=== second wave starts with need to recharge fuel, shield, ammo
                //=== twelth wave starts with need to recharge fuel, shield, ammo, except SEAKINGS
                if (ussrWave == 1 || (ussrWave == 11 && type != 3))
                    aircraft = [type, direction, x, y, 0, 0, 10, 10, 10, 'sit_on_deck', null, null, guid(), null];
                else
                    aircraft = [type, direction, x, y, 0, 0, GetTypeShieldMax(spriteTypeArray[type]), GetTypeFuelMax(spriteTypeArray[type]), GetTypeAmmoMax(spriteTypeArray[type]), 'sit_on_deck', null, null, guid(), null];

                sprite_deck.push(aircraft);
            }
        }
    }
}

function UpdateMissileUsa() {
    for (var i = 0; i < usaMissiles.length; i++) {
        var missile = usaMissiles[i];

        if (missile.IsBullet) {
            missile.x += 2 * tweenInverseMultiplier;
        }
        else {
            missile.x += game.rnd.pick(missileSpeedArray) * tweenInverseMultiplier; //missile speed wobble
            missile.y = GetHoverWobble(missile.y, missile.slot, 40, 30);
        }

        // if NO target present in slot, and shot not yet resolved, missile fades away
        if (missile.x >= (460 - missile.width) && missile.hit == null && ussrProfiles[missile.slot] == null) {
            missile.hit = false;
            DrawMissileFade(missile);
        }
        // if target present in slot, and shot not yet resolved
        if (missile.x >= (450 - missile.width)
            && missile.hit == null
            && ussrProfiles[missile.slot] != null) {

            //if hits
            if (game.rnd.pick(missileHitArray) || missile.IsBullet) {
                missile.hit = true;
                ussrProfiles[missile.slot][6] -= missile.damage; //missile damage to USSR health
                ussrHealthDelta = missile.damage;

                var isKill = false;
                //if ussr plane destroyed
                if (ussrProfiles[missile.slot][6] <= 0) {
                    isKill = true;
                    DestroyUssrChevron(missile.slot);
                    ussrProfiles[missile.slot][1].visible = false;
                    ussrProfiles[missile.slot][1].destroy;
                    ussrProfiles[missile.slot] = null;
                }

                DrawMissileHitText(missile.x + missile.width + 20, missile.y, 'Red', '-' + (missile.damage / 10).toString(), 32);
                DrawExplosion(missile.x + missile.width - 20, missile.y, isKill, (isKill) ? 3 : 1);

                if (ussrWaveHealth - ussrHealthDelta > 0) {
                    CameraFlash();

                    if (!missile.IsBullet) {
                        CameraShake();
                        CloudsSlide(missile.slot, 'usa');
                    }
                }

                missile.destroy(); //destroy missile
                usaMissiles.slice(i, 1);
            }
            else {
                ussrProfiles[missile.slot][3] = game.rnd.pick(['up', 'down']); //roll ussr plane that avoided missile
                ussrProfiles[missile.slot][2] = 0;   //reset roll counter
                missile.hit = false;
                DrawMissileHitText(missile.x + missile.width + 20, missile.y, 'Blue', 'miss', 32);
                DrawMissileFade(missile);
            }
        }

        //remove missiles off screen (missed or no target in slot)
        if (missile.x >= 800) {
            missile.destroy();
            usaMissiles.slice(i, 1);
        }
    }
}

function UpdateMissileUssr() {
    for (var i = 0; i < ussrMissiles.length; i++) {
        var missile = ussrMissiles[i];

        if (missile.IsBullet) {
            missile.x += 2 * tweenInverseMultiplier;
        }
        else {
            missile.x -= game.rnd.pick(missileSpeedArray) * tweenInverseMultiplier; //missile speed wobble
            missile.y = GetHoverWobble(missile.y, missile.slot, 40, 30);
        }

        // if target present in slot, and shot not yet resolved
        if (missile.x <= (250)
            && missile.hit == null
            && usaProfiles[missile.slot] != null) {

            var missileBlocked = false;
            // check if seaking and try to block
            if (usaProfiles[missile.slot][0] == 'seaking') {
                missileBlocked = game.rnd.pick([0, 1]) == 1;
            }
            //if seaking blocks
            if (missileBlocked) {
                missile.hit = false;
                DrawMissileHitText(missile.x + 10, missile.y, 'Blue', 'JAMMED', 32);
                DrawMissileDrop(missile);
            }
                //if hits
            else if (game.rnd.pick(missileHitArray) || missile.IsBullet) {
                missile.hit = true;

                var guid = usaProfiles[missile.slot][4];
                var deckArrayElement = findElement(sprite_deck, 12, guid);
                if (deckArrayElement) {
                    deckArrayElement.guid = guid;
                    deckArrayElement[6] -= missile.damage; //missile damage to USA plane                

                    var isKill = false;
                    //if usa plane destroyed
                    if (deckArrayElement[6] <= 0) {
                        isKill = true;
                        deckArrayElement[10].visible = false;
                        deckArrayElement[10].destroy;
                        deckArrayElement = null;

                        usaProfiles[missile.slot][1].visible = false;
                        usaProfiles[missile.slot][1].destroy;

                        ShuffleProfileStackDown(guid);
                    }
                }

                DrawMissileHitText(missile.x + 20, missile.y, 'Red', '-' + (missile.damage / 10).toString(), 32);
                DrawExplosion(missile.x - 20, missile.y, isKill, (isKill) ? 3 : 1);

                CameraFlash();
                if (!missile.IsBullet) {
                    CameraShake();
                    CloudsSlide(missile.slot, 'ussr');
                }

                missile.destroy(); //destroy missile
                ussrMissiles.slice(i, 1);
            }
            else {
                usaProfiles[missile.slot][3] = game.rnd.pick(['up', 'down']); //roll ussr plane that avoided missile
                usaProfiles[missile.slot][2] = 0;   //reset roll counter
                missile.hit = false;
                DrawMissileHitText(missile.x + 10, missile.y, 'Blue', 'miss', 32);
                DrawMissileFade(missile);
            }
        }

        // if USSR missile hit USA health
        if (missile.x <= 0
            && missile.hit == null) {
            missile.hit = true;
            // usaHealth -= missile.damage; //missile damage to USA health
            usaHealthDelta = missile.damage;

            DrawMissileHitText(missile.x + 10, missile.y, 'Red', '-' + (missile.damage / 10).toString(), 32);
            DrawExplosion(missile.x + 10, missile.y, false, 1);

            CameraFlash();

            if (!missile.IsBullet) {
                CameraShake();
                CloudsSlide(missile.slot, 'ussr');
            }

            missile.destroy(); //destroy missile
            ussrMissiles.slice(i, 1);
        }
    }
}

function SetupDeckSprites() {
    sprite_deck.forEach(function (sd) {
        SetupSpriteToggle(sd);
    });
}

function SetupSpriteToggle(deckSpriteObject) {

    var sprite = deckSpriteObject[10];

    sprite.inputEnabled = true; //  Enables all kind of input actions on this image (click, etc)
    sprite.input.useHandCursor = true;
    //attach some properties
    sprite.shieldMax = GetTypeShieldMax(spriteTypeArray[deckSpriteObject[0]]);
    sprite.fuelMax = GetTypeFuelMax(spriteTypeArray[deckSpriteObject[0]]);
    sprite.ammoMax = GetTypeAmmoMax(spriteTypeArray[deckSpriteObject[0]]);
    sprite.guid = deckSpriteObject[12];
    sprite.fuel = deckSpriteObject[7];
    sprite.type = spriteTypeArray[deckSpriteObject[0]];

    //EVENT (click isometric airplane)
    sprite.events.onInputDown.add(function (sprite) {
        game.iso.unproject(game.input.activePointer.position, cursorPos);
        DestroyAllSelections();
        spriteInfoPanelGuid = sprite.guid;
        DrawMoveIndicators(deckSpriteObject);
        DrawInfoPanel(sprite.type, 'usa');
        infoPanelTitleText = game.add.bitmapText(440, 725, 'fontRed', GetTypeText(sprite.type), 32);
        infoPanelTitleText.visible = true;

        //setup hover takeoff button
        if (sprite.fuel > 0 && isHoverFlightType(sprite.type)) {
            //add takeoff button
            spriteButton = game.add.sprite(sprite.x - 35, sprite.y - 55, 'tileset', 'vtol_arrow');
            spriteButton.scale.setTo(0.8, 0.8);
            spriteButton.anchor.set(-0, 0.5);

            spriteButton.inputEnabled = true; //  Enables all kind of input actions on this image (click, etc)
            spriteButton.input.useHandCursor = true;
            spriteButton.bringToTop();

            //EVENT (click takeoff button)
            spriteButton.events.onInputDown.add(function () {
                DestroyAllSelections();
                deckSpriteObject[9] = 'hover_up';
            });
            spriteButtons.push(spriteButton);
        }
    }, this);
}

function UpdateProfileStackUsa() {

    //get all in combat usa planes
    sprite_deck.forEach(function (spriteObject) {
        if (spriteObject[9] == 'away_from_carrier') {
            spriteObject[9] = 'in_combat';

            var firstSlot = spriteObject[13];

            if (usaProfiles[firstSlot]) {

                //set plane to return to carrier, if present (not destroyed)
                if (usaProfiles[0]) {
                    var returningPlaneGuid = usaProfiles[0][4];

                    sprite_deck.forEach(function (spriteObject) {
                        if (spriteObject[12] == returningPlaneGuid) {
                            spriteObject[9] = 'return_to_carrier';
                            spriteObject[13] = null; //fight slot empty
                        }
                    });
                }

                if (usaProfiles[0]) {
                    //returning plane animation
                    var returningPlane = game.add.sprite(350, 320, 'tileset', usaProfiles[0][0] + '_profile_level', usaProfileGroup);
                    returningPlane.scale.setTo(-1, 1);
                    var tweenReturningPlane = game.add.tween(returningPlane);
                    tweenReturningPlane.to({ x: -100 }, 600 * tweenMultiplier, Phaser.Easing.Quadratic.In, false);
                    tweenReturningPlane.start();
                    tweenReturningPlane.onComplete.add(function (sprite, tween) {
                        sprite.destroy();
                    });
                }
                DestroyUsaChevron(0);

                //shuffle profile stack down

                // get ref to sprite deck nad remove its slot
                if (usaProfiles[0]) {
                    var guid = usaProfiles[0][4];
                    var deckArrayElement = findElement(sprite_deck, 12, guid);
                    deckArrayElement[13] = null;

                    usaProfiles[0][1].destroy();
                    usaProfiles[0] = usaProfiles[1];
                    usaProfiles[1] = usaProfiles[2];
                    usaProfiles[2] = usaProfiles[3];

                    //reset assigned slots in profiles and sprite array
                    if (usaProfiles[0]) {
                        findElement(sprite_deck, 12, usaProfiles[0][4])[13] = 0;
                        var tweenPiece1 = game.add.tween(usaProfiles[0][1]);
                        tweenPiece1.to({ y: usaProfiles[0][1].y + 100 }, 350 * tweenMultiplier, Phaser.Easing.Quadratic.In, false);
                        tweenPiece1.start();
                    }

                    if (usaProfiles[1]) {
                        findElement(sprite_deck, 12, usaProfiles[1][4])[13] = 1;
                        var tweenPiece2 = game.add.tween(usaProfiles[1][1]);
                        tweenPiece2.to({ y: usaProfiles[1][1].y + 100 }, 350 * tweenMultiplier, Phaser.Easing.Quadratic.In, false);
                        tweenPiece2.start();
                    }

                    if (usaProfiles[2]) {
                        findElement(sprite_deck, 12, usaProfiles[2][4])[13] = 2;
                        var tweenPiece3 = game.add.tween(usaProfiles[2][1]);
                        tweenPiece3.to({ y: usaProfiles[2][1].y + 100 }, 350 * tweenMultiplier, Phaser.Easing.Quadratic.In, false);
                        tweenPiece3.start();
                    }

                    if (usaProfiles[3]) {
                        var tweenPiece4 = game.add.tween(usaProfiles[3][1]);
                        tweenPiece4.to({ y: usaProfiles[3][1].y + 100 }, 350 * tweenMultiplier, Phaser.Easing.Quadratic.In, false);
                        tweenPiece4.start();
                    }
                }
            }

            var profileSprite = game.add.sprite(0, 20 + (100 * (3 - firstSlot)), 'tileset', spriteTypeArray[spriteObject[0]] + '_profile_level', usaProfileGroup);
            profileSprite.inputEnabled = true;
            profileSprite.input.useHandCursor = true;
            profileSprite.guid = spriteObject[12];
            profileSprite.type = spriteTypeArray[spriteObject[0]];

            var chevronSprite = game.add.sprite(0, 20 + (100 * (3 - firstSlot)), 'tileset', 'nothing', usaProfileGroup);
            var chevronBlueSprite = game.add.sprite(0, 20 + (100 * (3 - firstSlot)), 'tileset', 'nothing', usaProfileGroup);

            // EVENT (return combat plane) 
            profileSprite.events.onInputDown.add(function (sprite) {

                //todo: refactor this with duplicate code above
                sprite_deck.forEach(function (spriteObject) {
                    if (spriteObject[12] == sprite.guid) {
                        spriteObject[9] = 'return_to_carrier';
                        spriteObject[13] = null;
                    }
                });

                ShuffleProfileStackDown(sprite.guid);
            }, this);

            //push plane to correct slot
            usaProfiles[firstSlot] =
            [
                spriteTypeArray[spriteObject[0]], //type
                profileSprite,
                0,
                'level',          //roll
                spriteObject[12], // guid
                null, // unused
                spriteObject[6],  // shield
                spriteObject[7],  // fuel
                spriteObject[8],   // ammo
                chevronSprite,     //
                chevronBlueSprite
            ];

            var tweenPieceIn = game.add.tween(usaProfiles[firstSlot][1]);
            tweenPieceIn.to({ x: 130 }, 350 * tweenMultiplier, Phaser.Easing.Quadratic.In, false);
            tweenPieceIn.start();
            tweenPieceIn.onComplete.add(function (sprite, tween) {
                if (sprite.type == 'foxbat_defect') {
                    ShuffleProfileFoxbatDown(spriteObject[12]);
                }
            });
        }
    });
}

function ShuffleProfileFoxbatDown(guid) {

    var lowestEmptySlot = null;

    //shuffle profile stack down
    for (var i = 0; i < usaProfiles.length; i++) {
        if (usaProfiles[i]) {
            if (usaProfiles[i][4] == guid) {
                //move foxbat down if gaps
                for (var j = i; j >= 0; j--) {
                    if (typeof (usaProfiles[j]) == 'undefined' || usaProfiles[j] == null) {
                        lowestEmptySlot = j;
                    }
                }
                if (lowestEmptySlot != null) {

                    //move foxbat profile sprite down to lowest slot
                    var tweenPiece = game.add.tween(usaProfiles[i][1]);
                    tweenPiece.to({ y: usaProfiles[i][1].y + (100 * (i - lowestEmptySlot)) }, 350 * tweenMultiplier, Phaser.Easing.Quadratic.In, false);
                    tweenPiece.start();
                    //move foxbat down in array
                    usaProfiles[lowestEmptySlot] = usaProfiles[i];
                    usaProfiles[i] = null;
                    findElement(sprite_deck, 12, usaProfiles[lowestEmptySlot][4])[13] = lowestEmptySlot;
                }
            }
        }
    }
}

function ShuffleProfileStackDown(guid) {
    //shuffle profile stack down
    for (var i = 0; i < usaProfiles.length; i++) {
        if (usaProfiles[i]) {
            if (usaProfiles[i][4] == guid) {

                // get ref to sprite deck and remove its slot               
                var deckArrayElement = findElement(sprite_deck, 12, guid);
                deckArrayElement[13] = null;

                usaProfiles[i][1].destroy(); //profile sprite destroy //todo: can we just load empty textures and kee the object in array?
                usaProfiles[i][9].destroy(); //chevron sprite destroy //todo: can we just load empty textures and kee the object in array?
                usaProfiles[i][10].destroy(); //chevron sprite destroy //todo: can we just load empty textures and kee the object in array?
                usaProfiles[i] = null;

                if (deckArrayElement[6] > 0) { //if still alive
                    //returning plane animation
                    var returningPlane = game.add.sprite(350, 20 + (100 * (3 - i)), 'tileset', spriteTypeArray[deckArrayElement[0]] + '_profile_level', usaProfileGroup);
                    returningPlane.scale.setTo(-1, 1);
                    var tweenReturningPlane = game.add.tween(returningPlane);
                    tweenReturningPlane.to({ x: -100 }, 600 * tweenMultiplier, Phaser.Easing.Quadratic.In, false);
                    tweenReturningPlane.start();
                    tweenReturningPlane.onComplete.add(function (sprite, tween) {
                        sprite.destroy();
                    });
                }

                //move all higher items down
                for (var j = i; j < usaProfiles.length; j++) {

                    usaProfiles[j] = usaProfiles[j + 1];
                    //reset assigned slot based on guid
                    if (usaProfiles[j]) {
                        findElement(sprite_deck, 12, usaProfiles[j][4])[13] = j;

                        if (usaProfiles[j]) {
                            var tweenPiece = game.add.tween(usaProfiles[j][1]);
                            tweenPiece.to({ y: usaProfiles[j][1].y + 100 }, 350 * tweenMultiplier, Phaser.Easing.Quadratic.In, false);
                            tweenPiece.start();
                        }
                    }
                }
            }
        }
    }
}

function UpdateUsaHealthBar(delta) {
    if (usaHealth > 0)
        usaHealth -= delta;

    var healthY = (usaFullHealth - usaHealth) / parseInt(usaFullHealth) * 400;

    usaHealthBar.y = healthY;

    if (healthY >= 400 & usaAlive) {

        DestroyAll();
        DestroyAllSelections();

        usaAlive = false;
        usaHealth = 0;
        ussrHealth = 0;
        ussrWaveHealth = 0;
        usaHealthBarDelta.visible = false;
        ussrHealthBarDelta.visible = false;

        if (!resetting) {
            DrawWaveText("  DEFEATED", "  GAME OVER", "");

            for (var i = 0; i < 200; i++) {
                game.time.events.add(Phaser.Timer.SECOND * (0.02 * i), function () {
                    DrawExplosion(
                        game.rnd.pick([0, 80, 160, 240, 320, 400, 480, 560, 640]),
                        game.rnd.pick([0, 120, 240, 360, 480, 600, 720, 840, 960]),
                        game.rnd.pick([true, false]),
                        game.rnd.pick([3, 4, 5, 6, 7, 8, 9]));
                });
            }
        }
    }

    //delta
    if (delta > 0 && usaHealth > 0) {
        usaHealthBarDelta.visible = true;
        usaHealthBarDelta.scale.setTo(2, (delta / parseInt(usaFullHealth) * 400));
        usaHealthBarDelta.y = healthY - ((delta / parseInt(usaFullHealth) * 400));

        game.time.events.add(Phaser.Timer.SECOND * 1, function () {
            usaHealthBarDelta.visible = false;
        });
    }
    usaHealthDelta = 0;
    resetting = false;
}

function UpdateUssrHealthBar(delta) {
    if (ussrWaveHealth > 0)
        ussrWaveHealth -= delta;

    var healthY = (ussrWaveFullHealth - ussrWaveHealth) / parseInt(ussrWaveFullHealth) * 400;

    ussrHealthBar.y = healthY;

    // delta
    if (delta > 0 && ussrWaveHealth > 0) {
        ussrHealthBarDelta.visible = true;
        ussrHealthBarDelta.scale.setTo(2, (delta / parseInt(ussrWaveFullHealth) * 400));
        ussrHealthBarDelta.y = healthY - ((delta / parseInt(ussrWaveFullHealth) * 400));

        game.time.events.add(Phaser.Timer.SECOND * 1, function () {
            ussrHealthBarDelta.visible = false;
        });
    }
    ussrHealthDelta = 0;
}

function UpdateUssrSubWave(wave, subwave) {
    var currentSubWaveArray = ussrWaves[wave][subwave];

    ussrProfiles = [];

    //reverse list so runs from top to bottom
    for (var i = currentSubWaveArray.length - 1; i >= 0; i--) {
        if (currentSubWaveArray[i]) {
            var type = currentSubWaveArray[i];
            var ussrPlaneProfileSprite = game.add.sprite(700, 20 + (i * 100), 'tileset', type + '_profile_level', ussrProfileGroup);
            ussrPlaneProfileSprite.inputEnabled = true;
            ussrPlaneProfileSprite.input.useHandCursor = true;
            ussrPlaneProfileSprite.type = type;
            ussrPlaneProfileSprite.missileDelay = GetTypeMissileDelay(type);
            ussrPlaneProfileSprite.slot = currentSubWaveArray.length - 1 - i;

            // EVENT (click USSR profile plane) 
            ussrPlaneProfileSprite.events.onInputDown.add(function (sprite) {
                DestroyAllSelections();
                DrawInfoPanel(sprite.type, 'ussr');
                infoPanelTitleText = game.add.bitmapText(440, 725, 'fontRed', GetTypeText(sprite.type), 32);
                infoPanelTitleText.visible = true;
                DrawMissileChevronStatusBar(sprite.missileDelay, 'ussr');
                infoPanelTextFireRate.x = 500;
                infoPanelTextFireRate.text = (sprite.missileDelay == 1) ? ":BULLET DELAY" : ":MISSILE DELAY";
                infoPanelText2.text = GetTypeText2(sprite.type);
                infoPanelText3.text = GetTypeText3(sprite.type);
                spriteInfoPanelSlot = sprite.slot;
            });

            var chevronSprite = game.add.sprite(600, 20 + (100 * (3 - ussrPlaneProfileSprite.slot)), 'tileset', 'nothing', ussrProfileGroup);

            //type, sprite, counter Roll, roll, guid, missileCounter, shield, fuel, ammo, missileDelayCounter
            ussrProfiles.push([ussrPlaneProfileSprite.type, ussrPlaneProfileSprite, 0, 'level', guid(), -250, GetTypeShieldMax(ussrPlaneProfileSprite.type), null, null, chevronSprite]);

            var tweenUssrPlaneProfileSprite = game.add.tween(ussrPlaneProfileSprite);
            tweenUssrPlaneProfileSprite.to({ x: 430 }, ((type == 'bear') ? 4000 : 600) * tweenMultiplier, Phaser.Easing.Quadratic.In, true, 4000);
            tweenUssrPlaneProfileSprite.onComplete.add(function (sprite, tween) {
                sprite[5] = 0; //reset counter
            });
        }
        else {
            ussrProfiles.push(null);
        }
    }
}

function AnimateProfile(spriteObjects, faction) {

    hawkEyeChevrons.forEach(function (sprite) {
        sprite.destroy();
    });
    seakingJammers.forEach(function (sprite) {
        sprite.destroy();
    });
    foxbatIntelMorseCodes.forEach(function (sprite) {
        sprite.destroy();
    });

    for (var i = 0; i < spriteObjects.length; i++) {

        if (faction == 'ussr') {
            foxbatIntelTexts[i].visible = false;

            if (foxbatIntel) {
                var intel = GetIntel(3 - i);
                if (intel && game.rnd.pick([true, false])) {
                    foxbatIntelTexts[i].text = intel;
                    foxbatIntelTexts[i].visible = true;
                }
            }
        }

        //usa: type, spriteProfileObject, ?, roll, guid, shield, fuel, ammo
        if (spriteObjects[i] != null && spriteObjects[i][0] != null) {

            spriteObjects[i][2]++;
            var type = spriteObjects[i][0];
            var sprite = spriteObjects[i][1];
            var counter = spriteObjects[i][2];
            var roll = spriteObjects[i][3];  // up, level, down

            spriteObjects[i][1].loadTexture('tileset', type + '_profile_' + roll);
            sprite.y = GetHoverWobble(sprite.y, i, 10, -10);

            if (counter + game.rnd.pick([0, 2, 5, -2, -5]) >= 60 && roll != 'level') {
                spriteObjects[i][3] = 'level';
                spriteObjects[i][1].loadTexture('tileset', type + '_profile_level');
                spriteObjects[i][2] = 0;
            }

            if (faction == 'usa') {
                // type, direction, x, y, destinationX, destinationY, shield, fuel, ammo, action, sprite, counter, guid, fightSlot

                var guid = spriteObjects[i][4];
                // get ref to sprite deck
                var deckArrayElement = findElement(sprite_deck, 12, guid);

                if (deckArrayElement) {

                    var ammoSize = GetTypeAmmoSize(spriteTypeArray[deckArrayElement[0]]);
                    if (ammoSize && deckArrayElement[11] <= 400) //todo: to be dynamic
                        deckArrayElement[11]++;//set counter up 1

                    deckArrayElement[7] -= fuelDecrease * tweenInverseMultiplier; //reduce fuel 

                    var isBullet = (ammoSize == 0.25);
                    var missileDelay = GetTypeMissileDelay(spriteTypeArray[deckArrayElement[0]]) - ((isBullet) ? 0 : missileDelayUnitModifier);
                    missileDelay = (missileDelay == 0 && !isBullet) ? 0.5 : missileDelay;  //make sure missile delay floor is not zero

                    //fire missiles
                    if (deckArrayElement[11] * tweenInverseMultiplier >= usaMissileDelaySize * missileDelay//counter
                        && deckArrayElement[8] > 0 //has ammo 
                        && ussrProfiles[deckArrayElement[13]]  //has ussr plane target
                        && ussrProfiles[deckArrayElement[13]][5] >= 0  //missile couter is not negative (delay wave beginning)
                        ) {

                        deckArrayElement[11] = 0; //counter to zero

                        //only bullets recoil
                        if (GetTypeAmmoSize(spriteTypeArray[deckArrayElement[0]]) == 0.25) {
                            var profileSprite = spriteObjects[i][1];
                            var tweenRecoil = game.add.tween(profileSprite);
                            profileSprite.x = 130;
                            var tweenRecoil = game.add.tween(profileSprite);
                            tweenRecoil.to({ x: profileSprite.x - 15 }, 100 * tweenMultiplier, Phaser.Easing.Linear.None, true);
                            tweenRecoil.start();

                            tweenRecoil.onComplete.add(function (sprite, tween) {
                                tweenRecoil = game.add.tween(profileSprite);
                                tweenRecoil.to({ x: profileSprite.x + 15 }, 500 * tweenMultiplier, Phaser.Easing.Linear.None, true, 100);
                            });
                        }

                        var missileObject = SetFlatSprite(200, 350 - (100 * deckArrayElement[13] - ((isBullet) ? 10 : 0)), 0, 'tileset', 'usa_missile_1', isoGroup);
                        missileObject.hit = null;
                        missileObject.damage = usaMissileDamage * ammoSize;
                        missileObject.slot = deckArrayElement[13];
                        missileObject.scale.setTo(ammoSize, ammoSize);
                        missileObject.IsBullet = isBullet;
                        usaMissiles.push(missileObject);

                        //reduce ammo by missiles damage (or bullet damage)
                        deckArrayElement[8] -= usaMissileDamage * ammoSize;

                        if (deckArrayElement[8] < 0)
                            deckArrayElement[8] = 0;
                    }

                    //fuel warning
                    if (deckArrayElement[7] < 102 && deckArrayElement[7] > 100) { //looks like an exact number match is not good
                        deckArrayElement[7] = 100;
                        DrawMissileHitText(spriteObjects[i][1].x, spriteObjects[i][1].y, 'Purple', 'bingo', 32);
                    }

                    //check if fuel empty                
                    if (deckArrayElement[7] <= 0) {

                        // no fuel death animation 
                        var tweenPiece = game.add.tween(spriteObjects[i][1]);
                        tweenPiece.to({
                            x: spriteObjects[i][1].x + 300,
                            y: 400,
                            angle: 25,
                            alpha: 0

                        }, 1000 * tweenMultiplier, null, false);
                        tweenPiece.start();

                        tweenPiece.onComplete.add(function (sprite, tween) {
                            sprite.destroy(); //destroy the sprite                       
                        }, this);

                        DestroyUsaChevron(i);

                        //remove the plane from usa sprites profile and deck
                        //todo: do we here need to reset assigned slots based on guids?
                        deckArrayElement = null;
                        spriteObjects[i] = null;
                        usaProfiles[i] = null

                        //move all higher items down
                        for (var j = i; j < usaProfiles.length; j++) {

                            usaProfiles[j] = usaProfiles[j + 1];

                            if (usaProfiles[j]) {
                                var guid = usaProfiles[j][4];
                                // get ref to sprite deck and remove its slot               
                                var deckArrayElement = findElement(sprite_deck, 12, guid);
                                deckArrayElement[13] -= 1;

                                var tweenPiece = game.add.tween(usaProfiles[j][1]);
                                tweenPiece.to({ y: usaProfiles[j][1].y + 100 }, 350 * tweenMultiplier, Phaser.Easing.Quadratic.In, false);
                                tweenPiece.start();
                            }
                        }
                    }
                    else
                        DrawStatusBarsUsa(
                            sprite.x,
                            sprite.y,
                            deckArrayElement[6], //shield
                            GetTypeShieldMax(type),
                            deckArrayElement[7], //fuel
                            GetTypeFuelMax(type),
                            deckArrayElement[8], //ammo
                            GetTypeAmmoMax(type),
                            graphicsProfile);

                    //if is armed aircraft type & has health & has fuel 
                    if (GetTypeAmmoSize(type) == 0.25 && deckArrayElement && deckArrayElement[6] > 0 && deckArrayElement[7] > 0) //bullets
                        DrawBulletChevrons(i, deckArrayElement[11], usaMissileDelaySize, GetTypeMissileDelay(spriteTypeArray[deckArrayElement[0]]), missileDelayUnitModifier, deckArrayElement[8], sprite.x, sprite.y);
                    else if (GetTypeAmmoSize(type) > 0 && deckArrayElement && deckArrayElement[6] > 0 && deckArrayElement[7] > 0)
                        DrawUsaMissileChevrons(i, deckArrayElement[11], usaMissileDelaySize, GetTypeMissileDelay(spriteTypeArray[deckArrayElement[0]]), missileDelayUnitModifier, deckArrayElement[8], sprite.x, sprite.y);


                    if (deckArrayElement != null) {
                        //hawkeye animation
                        if (spriteTypeArray[deckArrayElement[0]] == 'hawkeye') {
                            deckArrayElement[11]++;

                            if (deckArrayElement[11] <= 0.4 * usaMissileDelaySize) {
                                var hawkEyeChevronSpriteLeft = SetFlatSprite(spriteObjects[i][1].x + 30 - deckArrayElement[11], spriteObjects[i][1].y + 1, 0, 'tileset', 'chevron_5_5_blue');
                                hawkEyeChevronSpriteLeft.scale.setTo(-0.7, 0.7);
                                hawkEyeChevronSpriteLeft.bringToTop();
                                hawkEyeChevrons.push(hawkEyeChevronSpriteLeft);

                                var hawkEyeChevronSpriteRight = SetFlatSprite(spriteObjects[i][1].x + 80 + deckArrayElement[11], spriteObjects[i][1].y + 1, 0, 'tileset', 'chevron_5_5_blue');
                                hawkEyeChevronSpriteRight.scale.setTo(0.7, 0.7);
                                hawkEyeChevronSpriteRight.bringToTop();
                                hawkEyeChevrons.push(hawkEyeChevronSpriteRight);
                            }
                            else if (deckArrayElement[11] >= 1 * usaMissileDelaySize) {
                                deckArrayElement[11] = 0;
                            }
                        }

                        //seaking animation
                        if (spriteTypeArray[deckArrayElement[0]] == 'seaking') {
                            deckArrayElement[11]++;

                            if (deckArrayElement[11] <= 0.4 * usaMissileDelaySize) {
                                var seakingJammerSprite = SetFlatSprite(spriteObjects[i][1].x + 100 + deckArrayElement[11], spriteObjects[i][1].y + 20, 0, 'tileset', 'radar_jammer');
                                seakingJammerSprite.scale.setTo(1.5, 1.5);
                                seakingJammerSprite.bringToTop();
                                seakingJammers.push(seakingJammerSprite);
                            }
                            else if (deckArrayElement[11] >= 1 * usaMissileDelaySize) {
                                deckArrayElement[11] = 0;
                            }
                        }

                        //foxbat animation
                        if (spriteTypeArray[deckArrayElement[0]] == 'foxbat_defect' && foxbatIntel) {
                            if (game.rnd.pick([true, true, true, true, true, false])) {
                                var foxbatMorseCodeSprite = SetFlatSprite(spriteObjects[i][1].x + 35, spriteObjects[i][1].y, 0, 'tileset', 'sos');
                                foxbatMorseCodeSprite.scale.setTo(1.5, 4);
                                foxbatMorseCodeSprite.bringToTop();

                                foxbatIntelMorseCodes.push(foxbatMorseCodeSprite);
                            }
                        }
                    }
                }
            }

                //type, sprite, counterRoll, roll, guid, missileCounter, shield, fuel, ammo, missleDelayCounter
            else if (faction == 'ussr') {

                //defecting foxbat
                if (spriteObjects[i][0] == 'foxbat_defecting') {
                    if (sprite.x > 0) {
                        sprite.x -= 5 * tweenInverseMultiplier;
                    }
                    if (sprite.x <= 0) {

                        //reduce ussrHealth
                        ussrWaveHealth -= spriteObjects[i][6];

                        //destroy ussr foxbat profile item
                        spriteObjects[i][1].destroy();
                        spriteObjects[i] = null;
                        //add usa sprite_deck item
                        var foxbatDefector = [5, 0, -10, -10, 0, 0, 750, 250, 750, 'away_from_carrier', null, null, 'guid()', i];  //ste:todo: why this guid not work
                        DrawDeckSprite(foxbatDefector);
                        SetupSpriteToggle(foxbatDefector);
                        sprite_deck.push(foxbatDefector);
                    }
                }

                if (spriteObjects[i]) {
                    var shield = spriteObjects[i][6];
                    var shieldMax = GetTypeShieldMax(spriteObjects[i][0]);
                    // increase counter
                    spriteObjects[i][5]++;

                    if (GetTypeAmmoSize(spriteObjects[i][0]) > 0)
                        DrawUssrMissileChevrons(i, spriteObjects[i][5], ussrMissileDelaySize, GetTypeMissileDelay(spriteObjects[i][0]), spriteObjects[i][1].x, spriteObjects[i][1].y);

                    //foxbat defect
                    if (spriteObjects[i][0] == 'foxbat') {

                        var foxbatIsolated = true;
                        for (var x = 0; x < spriteObjects.length; x++) {

                            if (x != i)
                                if (spriteObjects[x] != null)
                                    foxbatIsolated = false;
                        }

                        // if foxbat isolated and is opposite an empty USA channel
                        if (foxbatIsolated && (typeof (usaProfiles[i]) == 'undefined' || usaProfiles[i] == null)) {
                            spriteObjects[i][2] += 1 * tweenInverseMultiplier;
                            if (spriteObjects[i][2] >= 800) {
                                spriteObjects[i][2] = 0;
                                spriteObjects[i][0] = 'foxbat_defecting';
                                DrawMissileHitText(sprite.x, sprite.y, 'Blue', 'DEFECTING', 32);
                                DestroyUssrChevron(i);
                            }
                        }
                    }

                    //fire russian missile
                    if (spriteObjects[i][0] != 'foxbat_defecting') {

                        DrawStatusBarsUssr(sprite.x, sprite.y, shield, shieldMax, graphicsStatusBars);

                        if (spriteObjects[i][5] * tweenInverseMultiplier >= (GetTypeMissileDelay(spriteObjects[i][0]) * ussrMissileDelaySize)) {

                            spriteObjects[i][5] = 0; //reset counter
                            var isBullet = false;
                            var ammoSize = GetTypeAmmoSize(spriteObjects[i][0]);
                            var missileObject = SetFlatSprite(445, 350 - (100 * i - ((isBullet) ? 10 : 0)), 0, 'tileset', 'ussr_missile_1', isoGroup);
                            missileObject.hit = null;
                            missileObject.damage = ussrMissileDamage * ammoSize;
                            missileObject.slot = i;
                            missileObject.scale.setTo(ammoSize, ammoSize);
                            missileObject.IsBullet = isBullet;
                            ussrMissiles.push(missileObject);
                        }
                    }
                }
            }
        }
    }
}

function SetHoverReturn(spriteObject) {

    spriteObject[9] = 'hover_return';

    var shield = spriteObject[6];
    var fuel = spriteObject[7];
    var ammo = spriteObject[8];

    var shieldAvailable = isDeckSpaceAvailable(shieldCoords[0], shieldCoords[1]);
    var fuelAvailable = isDeckSpaceAvailable(fuelCoords[0], fuelCoords[1]);
    var ammoAvailable = isDeckSpaceAvailable(ammoCoords[0], ammoCoords[1]);

    var shieldMax = GetTypeShieldMax(spriteTypeArray[spriteObject[0]]);
    var fuelMax = GetTypeFuelMax(spriteTypeArray[spriteObject[0]]);
    var ammoMax = GetTypeAmmoMax(spriteTypeArray[spriteObject[0]]);

    var shieldNeeded = shield < shieldMax;
    var fuelNeeded = fuel < fuelMax;
    var ammoNeeded = (ammoMax == 0) ? false : ammo < ammoMax;

    if (fuelAvailable && fuelNeeded && fuel <= shield && fuel <= ammo) {
        spriteObject[2] = fuelCoords[0];
        spriteObject[3] = fuelCoords[1];
        spriteObject[4] = fuelCoords[0];
        spriteObject[5] = fuelCoords[1];
        return;
    }
    else if (shieldAvailable && shieldNeeded && shield <= fuel && shield <= ammo) {
        spriteObject[2] = shieldCoords[0];
        spriteObject[3] = shieldCoords[1];
        spriteObject[4] = shieldCoords[0];
        spriteObject[5] = shieldCoords[1];
        return;
    }
    else if (ammoAvailable && ammoNeeded && ammo <= fuel && ammo <= shield) {
        spriteObject[2] = ammoCoords[0];
        spriteObject[3] = ammoCoords[1];
        spriteObject[4] = ammoCoords[0];
        spriteObject[5] = ammoCoords[1];
        return;
    }
    else if (fuelAvailable && fuelNeeded) {
        spriteObject[2] = fuelCoords[0];
        spriteObject[3] = fuelCoords[1];
        spriteObject[4] = fuelCoords[0];
        spriteObject[5] = fuelCoords[1];
        return;
    }
    else if (shieldAvailable && shieldNeeded) {
        spriteObject[2] = shieldCoords[0];
        spriteObject[3] = shieldCoords[1];
        spriteObject[4] = shieldCoords[0];
        spriteObject[5] = shieldCoords[1];
        return;
    }
    else if (ammoAvailable && ammoNeeded) {
        spriteObject[2] = ammoCoords[0];
        spriteObject[3] = ammoCoords[1];
        spriteObject[4] = ammoCoords[0];
        spriteObject[5] = ammoCoords[1];
        return;
    }

    // for each spot on deck
    for (var xIso = 0; xIso < 4; xIso++) {
        for (var yIso = 0; yIso < 8; yIso++) {
            //check is not catapult, catapult track, recharge spot, runway or lift
            if (!isOnCatapult(xIso, yIso)
                && !isOnCatapultTrack(xIso, yIso)
                && !isOnShield(xIso, yIso)
                && !isOnAmmo(xIso, yIso)
                && !isOnFuel(xIso, yIso)
                && !isOnRunway(xIso, yIso)
                && !isOnLift(xIso, yIso)
                && isDeckSpaceAvailable(xIso, yIso)
                ) {
                spriteObject[2] = xIso;
                spriteObject[3] = yIso;
                spriteObject[4] = xIso;
                spriteObject[5] = yIso;
                return;
            }
        }
    }
}

function RedirectSpritesOnInputDownEvent(xCoord, yCoord) {
    moveIndicators.forEach(function (sprite) {
        if (sprite && sprite.isoBounds.containsXY(xCoord, yCoord)) {
            sprite.events.onInputDown.dispatch(sprite, game.input.activePointer);
        }
    });
}

function RunMainLoop() {

    foxbatIntel = false;
    missileDelayUnitModifier = 0;

    for (var j = 0; j < sprite_deck.length; j++) {
        var sd = sprite_deck[j];

        //hawkeye missile speed buff
        if (spriteTypeArray[sd[0]] == 'hawkeye' && sd[9] == 'in_combat') {
            missileDelayUnitModifier++;
        }

        //foxbat defect intel
        if (spriteTypeArray[sd[0]] == 'foxbat_defect' && sd[9] == 'in_combat' && !foxbatIntel) {
            foxbatIntel = true;
        }

        if (sd[9] == 'sit_on_deck' || sd[9] == 'move_on_deck') {   //hack, status should be sit_on_deck when finished moving
            //recharge shield
            if (isOnShield(sd[2], sd[3])) {
                if (sd[6] < GetTypeShieldMax(spriteTypeArray[sd[0]])) {
                    sd[6] += shieldRefreshRate - (sd[6] % shieldRefreshRate); // prevent recharging out of sync with refresh rate '+' text test
                    if (sd[6] % 10 == 0)
                        DrawRechargeText(sd[10].x - 10 + game.rnd.pick([0, 15, 30, -15, -30]), sd[10].y - 20, 'Yellow', '+', 32);
                }
                if (sd[6] == GetTypeFuelMax(spriteTypeArray[sd[0]]) - shieldRefreshRate) {
                    DrawRechargeText(sd[10].x - 80, sd[10].y - 100, 'Yellow', '+', 256);
                }
            }
            //recharge fuel
            if (isOnFuel(sd[2], sd[3])) {
                if (sd[7] < GetTypeFuelMax(spriteTypeArray[sd[0]])) {
                    sd[7] += fuelRefreshRate - (sd[7] % fuelRefreshRate); // prevent recharging out of sync with refresh rate '+' text test               
                    if (sd[7] % 10 == 0)
                        DrawRechargeText(sd[10].x - 10 + game.rnd.pick([0, 15, 30, -15, -30]), sd[10].y - 20, 'Purple', '+', 32);
                }
                if (sd[7] == GetTypeFuelMax(spriteTypeArray[sd[0]]) - fuelRefreshRate) {
                    DrawRechargeText(sd[10].x - 80, sd[10].y - 100, 'Purple', '+', 256);
                }
            }
            //recharge ammo
            if (isOnAmmo(sd[2], sd[3])) {
                if (sd[8] < GetTypeAmmoMax(spriteTypeArray[sd[0]])) {
                    sd[8] += ammoRefreshRate - (sd[8] % ammoRefreshRate); // prevent recharging out of sync with refresh rate '+' text test
                    if (sd[8] % 10 == 0)
                        DrawRechargeText(sd[10].x - 10 + game.rnd.pick([0, 15, 30, -15, -30]), sd[10].y - 20, 'Red', '+', 32);
                }
                if (sd[8] == GetTypeFuelMax(spriteTypeArray[sd[0]]) - ammoRefreshRate) {
                    DrawRechargeText(sd[10].x - 80, sd[10].y - 100, 'Red', '+', 256);
                }
            }
        }

        if (sd[9] == 'hover_level') {
            sd[10].isoZ += game.rnd.pick([0, -0.5, 0.5]); // hover wobble
            sd[11] += 1;
            if (sd[11] + game.rnd.pick([0, 20, -20]) >= 40) {
                sd[9] = 'hover_leave';
                sd[11] = null;
            }
        }

        if (sd[9] == 'hover_leave') {
            sd[10].bringToTop();
            sd[11] += 1; //counter increases leaving speed (accelerates)

            if (sd[1] == 0) { // north
                sd[10].isoY -= 0.4 * sd[11] * tweenInverseMultiplier;
            }
            if (sd[1] == 1) { // east
                sd[10].isoX += 0.4 * sd[11] * tweenInverseMultiplier;
            }
            if (sd[1] == 2) { // south
                sd[10].isoY += 0.4 * sd[11] * tweenInverseMultiplier;
            }
            if (sd[1] == 3) { // west
                sd[10].isoX -= 0.4 * sd[11] * tweenInverseMultiplier;
            }

            // left carrier and is off screen
            if (sd[11] >= 100) {
                sd[11] = null;
                sd[9] = 'away_from_carrier';
                sd[13] = GetFirstSlot();

                if (sd[1] == 0) { // north
                    sd[10].isoY = 0;
                    sd[10].loadTexture('tileset', spriteTypeArray[sd[0]] + '_s');
                    sd[1] = 2; //face about
                }
                else if (sd[1] == 1) { // east
                    sd[10].isoX = 1500;
                    sd[10].loadTexture('tileset', spriteTypeArray[sd[0]] + '_w');
                    sd[1] = 3; //face about
                }
                else if (sd[1] == 2) { // south
                    sd[10].isoY = 1500;
                    sd[10].loadTexture('tileset', spriteTypeArray[sd[0]] + '_n');
                    sd[1] = 0; //face about
                }
                else if (sd[1] == 3) { // west
                    sd[10].isoX = -100;
                    sd[10].loadTexture('tileset', spriteTypeArray[sd[0]] + '_e');
                    sd[1] = 1; //face about
                }
            }
        }

        //set return plane to carrier
        if (sd[9] == 'return_to_carrier') {

            if (isNormalFlightType(spriteTypeArray[sd[0]])) {
                sd[9] = 'landing';
                sd[11] = 0;
                //set landing start position
                sd[10].isoX = 725;
                sd[10].isoY = 1900;
                sd[10].isoZ = 200;
                sd[10].bringToTop();

                //set recovery line will hit
                sd[10].recoveryLine = game.rnd.pick([0, 1, 2, 3]);
            }

            if (isHoverFlightType(spriteTypeArray[sd[0]])) {
                SetHoverReturn(sd);

                //north-south, shift plane onto correct line to land
                if (sd[1] == 0 || sd[1] == 2) {
                    sd[10].isoX = ((sd[4] * 2) + 1.5 + deckOffsetX) * isoBaseSize;
                }

                //east-west, shift plane onto correct line to land
                if (sd[1] == 1 || sd[1] == 3) {
                    sd[10].isoY = ((sd[5] * 2) + 1.5 + deckOffsetY) * isoBaseSize;
                }
            }
        }

        if (sd[9] == 'hover_return') {

            var delta = 0;
            if (sd[1] == 0) { // from north
                delta = tweenInverseMultiplier * 0.04 * (sd[10].isoY - ((2 * sd[5]) + deckOffsetY + 1.5) * isoBaseSize) + 1;
                sd[10].isoY -= delta;
            }
            if (sd[1] == 1) { // from east
                delta = tweenInverseMultiplier * 0.04 * ((((2 * sd[4]) + deckOffsetX + 1.5) * isoBaseSize) - sd[10].isoX) + 1;
                sd[10].isoX += delta;
            }
            if (sd[1] == 2) { // from south
                delta = tweenInverseMultiplier * 0.04 * ((((2 * sd[5]) + deckOffsetY + 1.5) * isoBaseSize) - sd[10].isoY) + 1;
                sd[10].isoY += delta;
            }
            if (sd[1] == 3) { //from west
                delta = tweenInverseMultiplier * 0.04 * (sd[10].isoX - ((2 * sd[4]) + deckOffsetX + 1.5) * isoBaseSize) + 1;
                sd[10].isoX -= delta;
            }

            if (delta < 1.02) {
                sd[9] = 'hover_level_return';
                sd[11] = 0;
            }
        }

        if (sd[9] == 'hover_level_return') {
            sd[10].isoZ += game.rnd.pick([0, -0.5, 0.5]); // hover wobble
            sd[11] += 1;
            if (sd[11] >= 40 * tweenMultiplier) {
                sd[9] = 'hover_down';
                sd[11] = null;
            }
        }

        if (sd[9] == 'hover_down') {
            sd[10].isoZ -= 2 * tweenInverseMultiplier;  //hover down 
            if (sd[10].isoZ <= deckSpriteIsoZ) { //hover limit 
                sd[9] = 'sit_on_deck';
                sd[11] = 0;
                sd[10].inputEnabled = true;
            }
        }

        if (sd[9] == 'ready_for_takeoff' && sd[11] == 0) {
            sd[1] = 0;  //set to head north   
            sd[10].loadTexture('tileset', spriteTypeArray[sd[0]] + '_n');
            sd[10].inputEnabled = false;
            sd[10].useHandCursor = false;
            sd[11] = 1;
        }

        //delay the catapult launch
        if (sd[9] == 'ready_for_takeoff' && sd[11] < 30) {
            sd[11] += 1;
        }

        //catapult launch
        if ((sd[9] == 'ready_for_takeoff' || sd[9] == 'takeoff') && sd[11] >= 30 && sd[11] < 100) {
            sd[9] = 'takeoff';
            sd[11] += 1;
            sd[10].isoX += 10 * tweenInverseMultiplier;
            sd[10].isoZ += 10 * tweenInverseMultiplier;
        }

        //catapult crash
        if ((sd[9] == 'ready_for_takeoff' || sd[9] == 'takeoff') && sd[11] == 35) {

            //check for cat crash
            for (var k = 0; k < sprite_deck.length; k++) {

                if (sprite_deck[k]
                    && isOnCatapultTrack(sprite_deck[k][2], sprite_deck[k][3])
                    && k != j //cannot collide with self
                    ) {
                    sprite_deck[j][2] = null;
                    sprite_deck[j][3] = null;
                    sprite_deck[k][2] = null;
                    sprite_deck[k][3] = null;

                    sprite_deck[j][9] = 'dead';
                    sprite_deck[k][9] = 'dead';
                    sprite_deck[j][10].kill();
                    sprite_deck[k][10].kill();

                    fireAnimation.visible = true;
                    fireAnimation.alpha = 1;
                    fireAnimation.x = sd[10].x - 20;
                    fireAnimation.y = sd[10].y - 50;
                    fireAnimation.animations.play('flames', 6, true);
                    fireAnimation.bringToTop();

                    game.time.events.add(Phaser.Timer.SECOND * 2, function () {
                        fireAnimation.visible = false;
                    });
                }
            }
        }

        //set normal flight away from carrier
        if (sd[9] == 'takeoff' && sd[11] >= 100) {
            sd[11] = 0;
            sd[9] = 'away_from_carrier';
            sd[13] = GetFirstSlot();
        }

        if (sd[9] == 'landing') {

            sd[11]++;

            //delay before checking and setting a crash
            if (sd[11] >= 50 * tweenMultiplier) {
                //check if crashing                
                for (var m = 0; m < sprite_deck.length; m++) {
                    var collisionOccurs = false;

                    if (sprite_deck[m] && m != j && isOnDeck(spriteActionArray[sprite_deck[m][0]])) //cannot collide with self
                    {
                        if (sprite_deck[m][2] == 1 && sprite_deck[m][3] == 7 && sprite_deck[m][9] == 'sit_on_deck') {
                            collisionOccurs = true;
                            sd[10].recoveryLine = 3; //ammo
                        }
                        else if (sprite_deck[m][2] == 1 && sprite_deck[m][3] == 6 && sd[10].recoveryLine <= 2 && sprite_deck[m][9] == 'sit_on_deck') {
                            collisionOccurs = true;
                            sd[10].recoveryLine = 2; //ammo
                        }
                        else if (sprite_deck[m][2] == 1 && sprite_deck[m][3] == 5 && sd[10].recoveryLine <= 1 && sprite_deck[m][9] == 'sit_on_deck') {
                            collisionOccurs = true;
                            sd[10].recoveryLine = 1; //ammo
                        }

                        if (collisionOccurs) {

                            var collisionDeckAircraftGuid = sprite_deck[m][12];
                            // var deckCollisionAircraft = findElement(sprite_deck, 12, collisionDeckAircraftGuid);
                            //deckCollisionAircraft[9] = 'dead';

                            //  deckCollisionAircraft[10].kill();

                            sd[9] = 'crashing';
                            sd[10].collisionDeckAircraftGuid = collisionDeckAircraftGuid;
                            sd[10].collisionIsoX = sprite_deck[m][10].isoX;
                            sd[10].collisionIsoY = sprite_deck[m][10].isoY;
                        }
                    }
                }
            }

            var delta = tweenInverseMultiplier * 0.02 * (sd[10].isoY - (14 + (2 * sd[10].recoveryLine) + deckOffsetY) * isoBaseSize) + 1;
            sd[10].isoY -= delta;
            sd[10].isoZ -= 0.02 * tweenInverseMultiplier;

            if (delta < 2 && !landingSequenceRunning) {
                landingSequenceRunning = true;
                //=== show recovery line text, then fade out
                switch (sd[10].recoveryLine) {

                    case 0:
                        recoveryFuelText.fontSize = 32;
                        recoveryFuelText.alpha = 0.5;
                        recoveryFuelText.text = 'fuel';

                        recoveryShieldText.fontSize = 32;
                        recoveryShieldText.alpha = 0.5;
                        recoveryShieldText.text = 'shield';

                        recoveryAmmoText.fontSize = 32;
                        recoveryAmmoText.alpha = 0.5;
                        recoveryAmmoText.text = 'ammo';
                        break;

                    case 1:
                        var maxFuel = GetTypeFuelMax(spriteTypeArray[sd[0]]);
                        sd[7] = (sd[7] + recoveryLineBonus >= maxFuel) ? maxFuel : sd[7] + recoveryLineBonus;
                        recoveryFuelText.hit = true;

                        recoveryFuelText.fontSize = 64;
                        recoveryFuelText.alpha = 1;
                        recoveryFuelText.text = 'fuel +' + (recoveryLineBonus / 10);

                        recoveryShieldText.fontSize = 32;
                        recoveryShieldText.alpha = 0.5;
                        recoveryShieldText.text = 'shield';

                        recoveryAmmoText.fontSize = 32;
                        recoveryAmmoText.alpha = 0.5;
                        recoveryAmmoText.text = 'ammo';
                        break;

                    case 2:
                        var maxShield = GetTypeShieldMax(spriteTypeArray[sd[0]]);
                        sd[6] = (sd[6] + recoveryLineBonus >= maxShield) ? maxShield : sd[6] + recoveryLineBonus;
                        recoveryShieldText.hit = true;

                        recoveryShieldText.fontSize = 64;
                        recoveryShieldText.alpha = 1;
                        recoveryShieldText.text = 'shield +' + (recoveryLineBonus / 10);

                        recoveryAmmoText.fontSize = 32;
                        recoveryAmmoText.alpha = 0.5;
                        recoveryAmmoText.text = 'ammo';
                        break;

                    case 3:
                        var maxAmmo = GetTypeAmmoMax(spriteTypeArray[sd[0]]);
                        sd[8] = (sd[8] + recoveryLineBonus >= maxAmmo) ? maxAmmo : sd[8] + recoveryLineBonus;
                        recoveryAmmoText.hit = true;

                        recoveryAmmoText.fontSize = 64;
                        recoveryAmmoText.alpha = 1;
                        recoveryAmmoText.text = 'ammo +' + (recoveryLineBonus / 10).toString();
                        break;
                }

                //AMMO
                game.time.events.add(Phaser.Timer.SECOND * 0.2, function () {
                    recoveryAmmoText.visible = true;
                    var tweenRecoveryAmmoText = game.add.tween(recoveryAmmoText);
                    tweenRecoveryAmmoText.to({ x: 100 }, (sd[10].recoveryLine == 3 ? 1500 : 1000) * tweenMultiplier, Phaser.Easing.Linear.None, false, 0);
                    tweenRecoveryAmmoText.start();
                    tweenRecoveryAmmoText.onComplete.add(function (sprite, tween) {
                        sprite.visible = false;
                        if (sprite.hit) {
                            landingSequenceRunning = false;
                        }
                    });
                });

                //SHIELD
                if (sd[10].recoveryLine != 3) {
                    game.time.events.add(Phaser.Timer.SECOND * 0.4, function () {
                        recoveryShieldText.visible = true;
                        var tweenRecoveryShieldText = game.add.tween(recoveryShieldText);
                        tweenRecoveryShieldText.to({ alpha: 0 }, (sd[10].recoveryLine == 2 ? 1500 : 1000) * tweenMultiplier, Phaser.Easing.Linear.None, false, 0);
                        tweenRecoveryShieldText.start();
                        tweenRecoveryShieldText.onComplete.add(function (sprite, tween) {
                            sprite.visible = false;
                            if (sprite.hit) {
                                landingSequenceRunning = false;
                            }
                        });
                    });
                }

                //FUEL
                if (sd[10].recoveryLine != 3 && sd[10].recoveryLine != 2) {
                    game.time.events.add(Phaser.Timer.SECOND * 0.6, function () {
                        recoveryFuelText.visible = true;
                        var tweenRecoveryFuelText = game.add.tween(recoveryFuelText);
                        tweenRecoveryFuelText.to({ alpha: 0 }, (sd[10].recoveryLine == 1 ? 1500 : 1000) * tweenMultiplier, Phaser.Easing.Linear.None, false, 0);
                        tweenRecoveryFuelText.start();
                        tweenRecoveryFuelText.onComplete.add(function (sprite, tween) {
                            sprite.visible = false;
                            if (sprite.hit) {
                                landingSequenceRunning = false;
                            }
                        });
                    });
                }

                //bolter
                if (sd[10].recoveryLine == 0) {
                    game.time.events.add(Phaser.Timer.SECOND * 0.8, function () {
                        recoveryBolterText.visible = true;
                        recoveryBolterText.alpha = 1;
                        var tweenRecoveryBolterText = game.add.tween(recoveryBolterText);
                        tweenRecoveryBolterText.to({ alpha: 0 }, 1500 * tweenMultiplier, Phaser.Easing.Linear.None, false, 0);
                        tweenRecoveryBolterText.start();
                        tweenRecoveryBolterText.onComplete.add(function (sprite, tween) {
                            sprite.visible = false;
                            landingSequenceRunning = false;
                        });
                    });
                }
            }
            if (delta < 1.02 && sd[10].recoveryLine != 0) {

                sd[9] = 'sit_on_deck';
                sd[11] = 0;
                sd[10].inputEnabled = true;

                //repostion on deck, chooses 1 of 3 cables
                sd[2] = 1;
                sd[3] = 4 + sd[10].recoveryLine;

                //repostion sprite
                sd[10].isoX = (sd[2] + (0.5 * (deckOffsetX + 1.5))) * isoGameSize;
                sd[10].isoY = (sd[3] + (0.5 * (deckOffsetY + 1.5))) * isoGameSize;
                sd[10].isoZ = deckSpriteIsoZ;
            }
            //bolter
            if (delta < 1.02 && sd[10].recoveryLine == 0) {
                sd[9] = 'ready_for_takeoff';
                sd[11] = 31;
            }
        }

        if (sd[9] == 'crashing') {

            var delta = tweenInverseMultiplier * 0.02 * (sd[10].isoY - (16 + (2 * sd[10].recoveryLine) + deckOffsetY) * isoBaseSize) + 1;
            sd[10].isoY -= delta;
            sd[10].isoZ -= 0.02 * tweenInverseMultiplier;

            if (delta < 1.2) {

                var deckCollisionAircraft = findElement(sprite_deck, 12, sd[10].collisionDeckAircraftGuid);

                deckCollisionAircraft[10].kill();
                deckCollisionAircraft[9] = 'dead';
                deckCollisionAircraft[2] = -1;
                deckCollisionAircraft[3] = -1;
                deckCollisionAircraft[4] = -1;
                deckCollisionAircraft[5] = -1;

                sd[9] = 'dead';
                sd[10].kill();

                fireAnimation.visible = true;
                fireAnimation.alpha = 1;
                fireAnimation.x = sprite_deck[j][10].x;
                fireAnimation.y = sprite_deck[j][10].y - 60;
                fireAnimation.animations.play('flames', 6, true);
                fireAnimation.bringToTop();

                game.time.events.add(Phaser.Timer.SECOND * 6, function () {
                    fireAnimation.visible = false;
                });
            }
        }

        if (sd[9] == 'hover_up') {
            sd[2] = 0;
            sd[3] = 0;
            sd[4] = 0;
            sd[5] = 0;
            sd[10].inputEnabled = false;
            sd[10].useHandCursor = false;
            sd[10].isoZ += 2 * tweenInverseMultiplier;  //hover rise 
            if (sd[10].isoZ >= 200) { //hover limit 
                sd[9] = 'hover_level';
                sd[11] = 0;
            }
        }

        //catch a moving plane thats 'dead' and make it properly dead, to stop blocking grid spots
        if (sd[9] == 'move_on_deck' && (sd[4] == -1 || sd[5] == -1 || sd[6] == 0))
            sd[9] = 'dead';

        if (sd[9] == 'move_on_deck' && sd[2] >= 0 && sd[3] >= 0 && sd[4] >= 0 && sd[5] >= 0) {

            // deck moving sprites
            //see: https://github.com/qiao/PathFinding.js
            var pathGridDeck = new PF.Grid(4, 8);

            for (var i = 0; i < sprite_deck.length; i++) {
                if (isOnDeck(sprite_deck[i][9]) && sprite_deck[i][2] >= 0 && sprite_deck[i][3] >= 0)
                    pathGridDeck.setWalkableAt(sprite_deck[i][2], sprite_deck[i][3], false);
            }
            var pathChosen = null;

            var destinationAvailable = true;
            var gridExpendableDeck = pathGridDeck.clone();
            var finder = new PF.AStarFinder();
            pathChosen = null;
            pathChosen = finder.findPath(sd[2], sd[3], sd[4], sd[5], gridExpendableDeck);


            if (pathChosen.length <= 1) {
                destinationAvailable = false;
                var gridExpendableDeck2 = pathGridDeck.clone();
                gridExpendableDeck2.setWalkableAt(sd[4], sd[5], true);
                pathChosen = null;
                pathChosen = finder.findPath(sd[2], sd[3], sd[4], sd[5], gridExpendableDeck2);
            }

            //=== if destination reachable, (of if not reachable, we can approach it)
            if (pathChosen.length > 2 || (pathChosen.length > 1 && destinationAvailable)) {
                //set piece to occupy next space
                var isoX = sd[10].isoX + (pathChosen[1][0] - sd[2]) * isoGameSize;
                var isoY = sd[10].isoY + (pathChosen[1][1] - sd[3]) * isoGameSize;

                //set sprite direction

                //n
                if (sd[3] > pathChosen[1][1])
                    sd[1] = 0;
                //e
                if (sd[2] < pathChosen[1][0])
                    sd[1] = 1;

                //s
                if (sd[3] < pathChosen[1][1])
                    sd[1] = 2;
                //w					
                if (sd[2] > pathChosen[1][0])
                    sd[1] = 3;

                sd[10].loadTexture('tileset', spriteTypeArray[sd[0]] + directionArray[sd[1]]);
                sd[10].guid = sd[12];
                sd[2] = pathChosen[1][0];
                sd[3] = pathChosen[1][1];
                sd[9] = 'busy_moving_on_deck';

                sd[10].xTo = sd[2];
                sd[10].yTo = sd[3];

                var tweenPiece = game.add.tween(sd[10]);
                tweenPiece.to({
                    isoX: isoX,
                    isoY: isoY
                }, 500 * tweenMultiplier, null, false);
                tweenPiece.start();

                tweenPiece.onComplete.add(function (sprite, tween) {
                    var deckArrayElement = findElement(sprite_deck, 12, sprite.guid);
                    deckArrayElement[9] = 'move_on_deck';
                    deckArrayElement[2] = sprite.xTo;
                    deckArrayElement[3] = sprite.yTo;

                    //has reached destination
                    if ((deckArrayElement[2] == deckArrayElement[4]) && (deckArrayElement[3] == deckArrayElement[5]))
                        deckArrayElement[9] = 'sit_on_deck';

                    if (IsCatapultLaunch(deckArrayElement[7], deckArrayElement[2], deckArrayElement[3], deckArrayElement[4], deckArrayElement[5])) {
                        deckArrayElement[9] = (isNormalFlightType(spriteTypeArray[deckArrayElement[0]])) ? 'ready_for_takeoff' : 'hover_up';
                        deckArrayElement[11] = 0;
                    }
                }, this);
            }
        }

        if (sd[9] == 'ready_for_takeoff' && sd[11] == 0) {
            sd[1] = 0;  //set to head north   
            sd[10].loadTexture('tileset', spriteTypeArray[sd[0]] + '_n');
            sd[11] = 1;
            sd.inputEnabled = false;
        }
    }
}

//========================== GET ======================

function GetUssrWaveHealth(wave) {
    var totalHealth = 0;
    ussrWaves[wave].forEach(function (subWave) {
        subWave.forEach(function (plane) {
            if (plane != null) {
                totalHealth += GetTypeShieldMax(plane);
            }
        });
    });
    return totalHealth;
}

function GetIntel(slot) {
    if (ussrWaves[ussrWave][ussrSubwave + 1]) {
        if (ussrWaves[ussrWave][ussrSubwave + 1][slot]) {
            return GetTypeIntelText(ussrWaves[ussrWave][ussrSubwave + 1][slot]);
        }
    }
    else if (ussrWaves[ussrWave + 1][0]) {
        if (ussrWaves[ussrWave + 1][0][slot])
            return GetTypeIntelText(ussrWaves[ussrWave + 1][0][slot]);
    }
    else
        return null;
}

function GetFirstSlot() {
    var firstSlot = 0;
    for (var i = 0; i < usaProfiles.length; i++) {
        if (usaProfiles[i]) {
            firstSlot++;
        }
    }
    return firstSlot > 3 ? 3 : firstSlot;
}

function GetTypeShieldMax(type) {
    switch (type) {
        case 'harrier':
            return 500;
        case 'hawkeye':
            return 250;
        case 'tomcat':
            return 750;
        case 'warthog':
            return 1000;
        case 'seaking':
            return 750;
        case 'foxbat_defect':
            return 750;
        case 'mig15':
            return 250;
        case 'mig21':
            return 500;
        case 'fulcrum':
            return 750;
        case 'foxbat':
            return 1000;
        case 'bear':
            return 1350;
        case 'huey':
            return 250;
    }
}

function GetTypeFuelMax(type) {
    switch (type) {
        case 'harrier':
            return 500;
        case 'hawkeye':
            return 1000;
        case 'tomcat':
            return 250;
        case 'warthog':
            return 500;
        case 'seaking':
            return 750;
        case 'foxbat_defect':
            return 250;
        case 'huey':
            return 150;
    }
}

function GetTypeAmmoMax(type) {
    switch (type) {
        case 'harrier':
            return 500;
        case 'hawkeye':
            return 0;
        case 'tomcat':
            return 1000;
        case 'warthog':
            return 975;
        case 'seaking':
            return 0;
        case 'foxbat_defect':
            return 750;
        case 'foxbat_defecting':
            return 0;
        case 'huey':
            return 0;
    }
}

function GetTypeMissileDelay(type) {
    switch (type) {
        case 'harrier':
            return 5;
        case 'hawkeye':
            return 0;
        case 'tomcat':
            return 4;
        case 'warthog':
            return 1; //bullets
        case 'seaking':
            return 0;
        case 'foxbat_defect':
            return 3;
        case 'huey':
            return 500;
        case 'mig15':
            return 5;
        case 'mig21':
            return 4;
        case 'fulcrum':
            return 3;
        case 'foxbat':
            return 2;
        case 'foxbat_defecting':
            return 0;
        case 'bear':
            return 5;
    }
}

function GetTypeAmmoSize(type) {
    switch (type) {
        case 'harrier':
            return 1;
        case 'hawkeye':
            return 0;
        case 'tomcat':
            return 1;
        case 'warthog':
            return 0.25; //bullets
        case 'seaking':
            return 0;
        case 'foxbat_defect':
            return 1;
        case 'huey':
            return 0;
        case 'mig15':
            return 1;
        case 'mig21':
            return 1;
        case 'fulcrum':
            return 1;
        case 'foxbat':
            return 1;
        case 'bear':
            return 2;
    }
}

function GetTypeText(type) {
    switch (type) {
        case 'harrier':
            return 'AV-8B Harrier';
        case 'hawkeye':
            return 'E-2 Hawkeye';
        case 'tomcat':
            return 'F-14 Tomcat';
        case 'warthog':
            return 'A-10 Thunderbolt';
        case 'seaking':
            return 'SH-3 Seaking';
        case 'foxbat_defect':
            return 'MiG-25 Foxbat';
        case 'huey':
            return 'UH-1 Bell Huey';
        case 'mig15':
            return 'MiG-15 Fagot';
        case 'mig21':
            return 'MiG-21 Fishbed';
        case 'fulcrum':
            return 'Mig-29 Fulcrum';
        case 'foxbat':
            return 'MiG-25 Foxbat';
        case 'bear':
            return 'TU-95 Bear';
    }
}

function GetTypeIntelText(type) {
    switch (type) {
        case 'mig15':
            return '15';
        case 'mig21':
            return '21';
        case 'fulcrum':
            return '29';
        case 'foxbat':
            return '25';
        case 'bear':
            return '95';
    }
}

function GetTypeText2(type) {
    switch (type) {
        case 'harrier':
            return 'Small all-rounder VTOL';
        case 'hawkeye':
            return 'Decrease missile delay of';
        case 'tomcat':
            return 'Fast, powerful interceptor';
        case 'warthog':
            return 'Heavy-armour. 30mm Gatling';
        case 'seaking':
            return 'Electronic missile-jamming';
        case 'foxbat_defect':
            return 'DEFECTOR. Can gather intel';
        case 'huey':
            return 'Multi-role chopper doing';
        case 'mig15':
            return 'Small 1950s 1st Gen';
        case 'mig21':
            return 'Medium 1960s 3rd Gen';
        case 'fulcrum':
            return 'Large 1970s 4th Gen';
        case 'foxbat':
            return 'V.fast, heavy, short range';
        case 'bear':
            return 'Long-range bomber';
    }
}

function GetTypeText3(type) {
    switch (type) {
        case 'harrier':
            return 'fighter-bomber';
        case 'hawkeye':
            return 'armed craft by 1 chevron';
        case 'tomcat':
            return 'but short-ranged';
        case 'warthog':
            return 'cannon never misses';
        case 'seaking':
            return 'VTOL helicopter';
        case 'foxbat_defect':
            return 'on enemies future movements';
        case 'huey':
            return 'evac. Ditch it ASAP';
        case 'mig15':
            return 'jet-fighter';
        case 'mig21':
            return 'jet-figter';
        case 'fulcrum':
            return 'jet-fighter';
        case 'foxbat':
            return 'WILL DEFECT IF ISOLATED!';
        case 'bear':
            return 'Beware of its ICBM!';
    }
}

function GetHoverWobble(y, slot, yMax, yMin) {
    if (y > yMax + (100 * (3 - slot)))
        y += game.rnd.pick([0, -0.5, -0.5, -0.5, 0.5]);
    else if (y < yMin + (100 * (3 - slot)))
        y += game.rnd.pick([0, -0.5, 0.5, 0.5, 0.5]);
    else
        y += game.rnd.pick([0, -0.5, 0.5]);

    return y;
}

function GetSelectedLevel(x, y) {

    // Calculate the corners of the menu
    var x1 = 170;
    var x2 = 520;
    var y1 = 30;
    var y2 = 404;
    var buttonSize = 74;

    // Check if the click was inside the menu
    if (x > x1 && x < x2 && y > y1 && y < y2) {

        //1,2,3,4
        if (y > 48 && y < 48 + buttonSize) {
            if (x > 188 && x < 188 + buttonSize) {
                return 0;
            }
            if (x > 264 && x < 264 + buttonSize) {
                return 1;
            }
            if (x > 344 && x < 344 + buttonSize) {
                return 2;
            }
            if (x > 422 && x < 422 + buttonSize) {
                return 3;
            }
        }
        //5,6,7,8
        if (y > 126 && y < 126 + buttonSize) {
            if (x > 188 && x < 188 + buttonSize) {
                return 4;
            }
            if (x > 264 && x < 264 + buttonSize) {
                return 5;
            }
            if (x > 344 && x < 344 + buttonSize) {
                return 6;
            }
            if (x > 422 && x < 422 + buttonSize) {
                return 7;
            }
        }
        //9,10,11,12
        if (y > 202 && y < 202 + buttonSize) {
            if (x > 188 && x < 188 + buttonSize) {
                return 8;
            }
            if (x > 264 && x < 264 + buttonSize) {
                return 9;
            }
            if (x > 344 && x < 344 + buttonSize) {
                return 10;
            }
            if (x > 422 && x < 422 + buttonSize) {
                return 11;
            }
        }
        //13,14,15,16
        if (y > 278 && y < 278 + buttonSize) {
            if (x > 188 && x < 188 + buttonSize) {
                return 12;
            }
            if (x > 264 && x < 264 + buttonSize) {
                return 13;
            }
            if (x > 344 && x < 344 + buttonSize) {
                return 14;
            }
            if (x > 422 && x < 422 + buttonSize) {
                return 15;
            }
        }

        //17,18,19,20
        if (y > 354 && y < 354 + buttonSize) {
            if (x > 188 && x < 188 + buttonSize) {
                return -2;  // music on
            }
            if (x > 264 && x < 264 + buttonSize) {
                return -3;  // music off
            }
        }
    }
    return -1;
}

//========================== TEST ======================
function isUpdateUssrWave() {
    if (ussrWaves[ussrWave + 1])
        return true;
    return false;
}

function isUpdateUssrSubWave() {
    if (ussrWaves[ussrWave][ussrSubwave + 1] && !resetting)
        return true;
    return false;
}

function IsUssrProfileStillExist() {
    var profilesStillExist = false;
    ussrProfiles.forEach(function (profile) {
        if (profile != null)
            profilesStillExist = true;
    });
    return profilesStillExist;
}


function IsCatapultLaunch(fuel, x, y, xTo, yTo) {
    if (fuel > 0
        && isOnCatapult(x, y)
        && x == xTo
        && y == yTo
        ) return true;

    return false;
}

function isDeckSpaceAvailable(x, y) {
    var deckSpaceAvailable = true;
    sprite_deck.forEach(function (sd) {
        if ((sd[2] == x && sd[3] == y) || (sd[4] == x && sd[5] == y) && isOnDeck(sd[9])) {
            deckSpaceAvailable = false;
        }
    });
    return deckSpaceAvailable;
}

function isHoverFlightType(type) {
    return type == 'harrier' || type == 'seaking' || type == 'huey';
}

function isNormalFlightType(type) {
    return type == 'hawkeye' || type == 'tomcat' || type == 'warthog' || type == 'foxbat_defect';
}

function isOnCatapult(x, y) {
    return catapultCoords[0] == x && catapultCoords[1] == y;
}

function isOnCatapultTrack(x, y) {
    return catapultTrackCoords[0] == x && catapultTrackCoords[1] == y;
}

function isOnShield(x, y) {
    return shieldCoords[0] == x && shieldCoords[1] == y;
}

function isOnRunway(x, y) {
    return (x == 1 && y == 6)
    || (x == 1 && y == 7)
    || (x == 1 && y == 8);
}

function isOnLift(x, y) {
    return liftCoords[0] == x && liftCoords[1] == y;
}

function isOnFuel(x, y) {
    return fuelCoords[0] == x && fuelCoords[1] == y;
}

function isOnAmmo(x, y) {
    return ammoCoords[0] == x && ammoCoords[1] == y;
}

function isOnDeck(action) {
    return action == 'sit_on_deck'
        || action == 'move_on_deck'
        || action == 'busy_moving_on_deck'
        || action == 'hover_down'
        || action == 'hover_level_return'
        || action == 'hover_return';
};

//========================== DRAW ==============================
function DrawMenuSelection(ussrWave) {

    ussrWave += 1;
    // Calculate the corners of the menu
    var x = 185;
    var y = 45;

    if (ussrWave == 2 || ussrWave == 6 || ussrWave == 10 || ussrWave == 14)
        x = 263;
    if (ussrWave == 3 || ussrWave == 7 || ussrWave == 11 || ussrWave == 15)
        x = 341;
    if (ussrWave == 4 || ussrWave == 8 || ussrWave == 12 || ussrWave == 16)
        x = 419;

    if (ussrWave == 5 || ussrWave == 6 || ussrWave == 7 || ussrWave == 8)
        y = 122;
    if (ussrWave == 9 || ussrWave == 10 || ussrWave == 11 || ussrWave == 12)
        y = 198;
    if (ussrWave == 13 || ussrWave == 14 || ussrWave == 15 || ussrWave == 16)
        y = 276;

    var buttonSize = 74;

    graphicsProfile.lineStyle(10, 0xFF0000, 1);
    graphicsProfile.drawRect(x, y, buttonSize, buttonSize);
    graphicsProfile.endFill();
    game.world.bringToTop(graphicsProfile);
}

function DrawHealthBarsAnimateUp() {
    ussrHealthBar.y = 400;
    usaHealthBar.y = 400;
    var tweenUssrHealthBar = game.add.tween(ussrHealthBar);
    tweenUssrHealthBar.to({ y: 0 }, 2000 * tweenMultiplier, Phaser.Easing.Quadratic.In, false);
    tweenUssrHealthBar.start();

    var tweenUsaHealthBar = game.add.tween(usaHealthBar);
    tweenUsaHealthBar.to({ y: 0 }, 2000 * tweenMultiplier, Phaser.Easing.Quadratic.In, false);
    tweenUsaHealthBar.start();
}

function DrawUsaMissileChevrons(profileSlot, counter, usaMissileDelaySize, missileDelayUnits, missileDelayUnitModifier, ammo, x, y) {
    var chevronsTotal = missileDelayUnits - missileDelayUnitModifier;
    var chevronSelected = (ammo <= 0) ? 0 : Math.floor(parseInt((counter * tweenInverseMultiplier) + 20) / parseInt(usaMissileDelaySize));
    var chevronSelectedBugFix = (chevronSelected > chevronsTotal) ? chevronsTotal : chevronSelected //this shouldn't happen
    usaProfiles[profileSlot][9].x = x + 32;
    usaProfiles[profileSlot][9].y = y + 64;

    if (chevronSelectedBugFix >= 0 && chevronsTotal > 0) {
        usaProfiles[profileSlot][9].loadTexture('tileset', 'chevron_' + chevronSelectedBugFix + '_' + chevronsTotal);
    }

    if (missileDelayUnitModifier > 0) {
        usaProfiles[profileSlot][10].loadTexture('tileset', 'chevron_' + missileDelayUnitModifier + '_' + missileDelayUnitModifier + '_blue');
        usaProfiles[profileSlot][10].x = usaProfiles[profileSlot][9].x + (16 * chevronsTotal);
        usaProfiles[profileSlot][10].y = usaProfiles[profileSlot][9].y;
    }
    else {
        usaProfiles[profileSlot][10].loadTexture('tileset', 'nothing');
    }
}

function DrawUssrMissileChevrons(profileSlot, counter, ussrMissileDelaySize, missileDelayUnits, x, y) {
    var chevronsTotal = missileDelayUnits;
    var chevronSelected = Math.floor(parseInt((counter * tweenInverseMultiplier) + 20) / parseInt(ussrMissileDelaySize));
    ussrProfiles[profileSlot][9].x = x + 100;
    ussrProfiles[profileSlot][9].y = y + 64;
    ussrProfiles[profileSlot][9].scale.setTo(-1, 1);

    var chevronSelectedBugFix = (chevronSelected > chevronsTotal) ? chevronsTotal : chevronSelected //this shouldn't happen
    chevronSelectedBugFix = (chevronSelectedBugFix < 0) ? 0 : chevronSelectedBugFix;
    ussrProfiles[profileSlot][9].loadTexture('tileset', 'chevron_' + chevronSelectedBugFix + '_' + chevronsTotal);
}

function DrawBulletChevrons(profileSlot, counter, usaMissileDelaySize, missileDelayUnits, missileDelayUnitModifier, ammo, x, y) {
    var bullets = (ammo <= 0) ? 0 : ((counter * tweenInverseMultiplier) + 20) >= (usaMissileDelaySize) ? 1 : 0;
    usaProfiles[profileSlot][9].x = x + 32;
    usaProfiles[profileSlot][9].y = y + 64;
    usaProfiles[profileSlot][9].loadTexture('tileset', 'chevron_bullet_' + bullets);
}

function DrawMissileChevronStatusBar(chevrons, faction) {

    if (chevrons && chevrons > 0)
        missileChevronStatusBar.loadTexture('tileset', 'chevron_' + chevrons + '_' + chevrons);
    else
        missileChevronStatusBar.loadTexture('tileset', 'nothing');

    if (faction == 'usa') {
        missileChevronStatusBar.scale.setTo(1, 1);
        missileChevronStatusBar.x = 580;
    }
    else {
        missileChevronStatusBar.scale.setTo(-1, 1);
        missileChevronStatusBar.x = 500;
    }
    missileChevronStatusBar.bringToTop();
}

function DrawBulletChevronStatusBar(faction) {
    missileChevronStatusBar.loadTexture('tileset', 'chevron_bullet_1');

    if (faction == 'usa') {
        missileChevronStatusBar.scale.setTo(1, 1);
        missileChevronStatusBar.x = 580;
    }
    else {
        missileChevronStatusBar.scale.setTo(-1, 1);
        missileChevronStatusBar.x = 480;
    }

    missileChevronStatusBar.bringToTop();
}

function DrawExplosion(x, y, isKill, scale) {
    var explosion = SetFlatSprite(x, y, 0, 'tileset', 'explosion', isoGroup);
    if (isKill) {
        explosion.y -= 30;
    }
    explosion.scale.setTo(scale, scale);
    explosion.bringToTop();
    var tweenExplosion = game.add.tween(explosion);
    tweenExplosion.to({ alpha: 0 }, (isKill ? 2000 : 500) * tweenMultiplier, null, false);
    tweenExplosion.start();
    tweenExplosion.onComplete.add(function (sprite, tween) {
        sprite.destroy();
    }, this);
}

function DrawStatusBarsUssr(x, y, shield, shieldMax, graphicsProfile) {

    graphicsProfile.lineStyle(0, 0x000000, 1);
    graphicsProfile.beginFill(0xF0F000);
    graphicsProfile.drawRect(x + 130, y + 10, (shield / 10), 30);
    graphicsProfile.endFill();

    graphicsProfile.lineStyle(2, 0x000000, 1);
    graphicsProfile.drawRect(x + 130, y + 10, (shieldMax / 10), 30);
    graphicsProfile.endFill();
}

function DrawStatusBarsUsa(x, y, shield, shieldMax, fuel, fuelMax, ammo, ammoMax, graphicsProfile) {

    graphicsProfile.lineStyle(0, 0x000000, 1);

    if (ammoMax > 0) {
        graphicsProfile.beginFill(0xFF0000);
        graphicsProfile.drawRect(x - 10 - (ammo / 10), y + 10, (ammo / 10), 10);
    }

    graphicsProfile.beginFill(0xF0F000);
    graphicsProfile.drawRect(x - 10 - (shield / 10), y + 20, (shield / 10), 10);
    graphicsProfile.beginFill(0x800080);
    graphicsProfile.drawRect(x - 10 - (fuel / 10), y + 30, (fuel / 10), 10);
    graphicsProfile.endFill();

    graphicsProfile.lineStyle(2, 0x000000, 1);

    if (ammoMax > 0)
        graphicsProfile.drawRect(x - 10 - (ammoMax / 10), y + 10, (ammoMax / 10), 10);

    graphicsProfile.drawRect(x - 10 - (shieldMax / 10), y + 20, (shieldMax / 10), 10);
    graphicsProfile.drawRect(x - 10 - (fuelMax / 10), y + 30, (fuelMax / 10), 10);

    graphicsProfile.endFill();
}

function CameraShake() {
    game.camera.shake(0.02, 100);
}

function CameraFlash() {
    game.camera.flash(0xFF0000, 100);
}

function CloudsSlide(missileSlot, faction) {
    var cloudSprite;
    switch (missileSlot) {

        case 0: cloudSprite = cloud_4;
            break;
        case 1: cloudSprite = cloud_3;
            break;
        case 2: cloudSprite = cloud_2;
            break;
        case 3: cloudSprite = cloud_1;
            break;
    }
    var x = (faction == 'ussr') ? -50 : 50;

    var tweenCloudOut = game.add.tween(cloudSprite);
    var tweenCloudIn = game.add.tween(cloudSprite);
    tweenCloudOut.to({ x: cloudSprite.x + x }, 100 * tweenMultiplier, Phaser.Easing.Linear.None, true);
    tweenCloudOut.onComplete.add(function (sprite, tween) {
        tweenCloudIn.to({ x: cloudSprite.x - x }, 1500 * tweenMultiplier, Phaser.Easing.Linear.None, true, 500);
    });

    //ensure cloud returns to starting position to prevent it staying misplaced after a tug-of-war
    tweenCloudIn.onComplete.add(function (sprite, tween) {
        sprite.x = -50;
    });
}

function DrawInfoPanel(type, faction) {
    spriteInfoPanelBackground.visible = true;
    spriteInfoPanelBackground.bringToTop();
    spriteInfoPanelProfile.x = (faction == 'ussr') ? 400 : 560;
    spriteInfoPanelProfile.loadTexture('tileset', type + '_profile_level');
    spriteInfoPanelProfile.bringToTop();
}

function DrawInsigniaPanel(number) {
    if (number > 0) {

        mainTitleLine1Text.text = '   defcon ' + number;
        mainTitleLine1Text.visible = true;
        //mainTitleLine1Text.bringToTop();
        mainTitleLine2Text.text = '   victory';
        mainTitleLine2Text.visible = true;
        //mainTitleLine2Text.bringToTop();
        mainTitleLine3Text.text = '   continue';
        mainTitleLine3Text.visible = true;
        //mainTitleLine3Text.bringToTop();

        textGroup.visible = false;

        graphicsProfile.lineStyle(2, 0x999999, 0);
        graphicsProfile.beginFill(0x999999);
        graphicsProfile.drawRect(0, 390, 700, 510);
        graphicsProfile.endFill();
        game.world.bringToTop(graphicsProfile);

        spriteInsigniaPanel.visible = true;
        spriteInsigniaPanel.loadTexture('insigniaset', 'insignia_' + number);
        spriteInsigniaPanel.bringToTop();
    }
}

function DestroyInsigniaPanel() {
    spriteInsigniaPanel.visible = false;
}

function DrawLevelTextInfoPanel(ussrWave) {

    spriteInfoPanelBackground.visible = true;
    spriteInfoPanelBackground.bringToTop();
    admiral.bringToTop();
    admiral.visible = true;

    if (ussrWave >= 0 && ussrWave <= 15)
        infoPanelLevelText1.text = "  ===== DEFCON:" + (ussrWave + 1) + " =====";
    else
        infoPanelLevelText1.text = "  ===== victory =====";

    mouthAnimation.visible = true;
    mouthAnimation.animations.play('mouths', 1, true);
    mouthAnimation.bringToTop();

    if (levelText[ussrWave]) {
        if (levelText[ussrWave][0])
            infoPanelLevelText2.text = levelText[ussrWave][0];
        if (levelText[ussrWave][1])
            infoPanelLevelText3.text = levelText[ussrWave][1];
        if (levelText[ussrWave][2])
            infoPanelLevelText4.text = levelText[ussrWave][2];
        if (levelText[ussrWave][3])
            infoPanelLevelText5.text = levelText[ussrWave][3];
        if (levelText[ussrWave][4])
            infoPanelLevelText6.text = levelText[ussrWave][4];
    }
}

function SetGround(x, y, z, tileAtlas, tileName, group) {
    return game.add.isoSprite(x * isoBaseSize, y * isoBaseSize, z, tileAtlas, tileName, group);
}

function SetSprite(x, y, z, tileAtlas, tileName, group) {
    return game.add.isoSprite((x * isoGameSize) + (1.5 * isoBaseSize), (y * isoGameSize) + (1.5 * isoBaseSize), z, tileAtlas, tileName, group);
}

function SetFlatSprite(x, y, z, tileAtlas, tileName, group) {
    return game.add.sprite(x, y, tileAtlas, tileName, group);
}

function DrawHud() {

    hudCounter++;
    if (hudCounter >= hudCounterThreshold) {
        hudCounter = 0;
        hudSprite.x = 100;
        hudSprite.y = 0;
        hudSprite.alpha = 0;
        hudSprite.visible = true;
        hudSprite.bringToTop();

        var tweenHudFloat = game.add.tween(hudSprite);
        tweenHudFloat.to({ alpha: 1, x: 100 + game.rnd.pick(hudFloatDirection), y: 0 + game.rnd.pick(hudFloatDirection) }, 200, null, false);
        tweenHudFloat.start();
        tweenHudFloat.onComplete.add(function (sprite, tween) {
            var tweenHudFloat2 = game.add.tween(sprite);
            tweenHudFloat2.to({ alpha: 0.5, x: sprite.x + game.rnd.pick(hudFloatDirection), y: sprite.y + game.rnd.pick(hudFloatDirection) }, 300, null, false);
            tweenHudFloat2.start();
            tweenHudFloat2.onComplete.add(function (sprite, tween) {
                var tweenHudFloat3 = game.add.tween(sprite);
                tweenHudFloat3.to({ alpha: 0, x: sprite.x + game.rnd.pick(hudFloatDirection), y: sprite.y + game.rnd.pick(hudFloatDirection) }, 800, null, false);
                tweenHudFloat3.start();
                tweenHudFloat3.onComplete.add(function (sprite, tween) {
                    sprite.visible = false;
                    sprite.alpha = 0;
                });
            });
        });
    }
}

function DrawSpriteInfoPanel() {
    if (spriteInfoPanelGuid) {
        var deckArrayElement = findElement(sprite_deck, 12, spriteInfoPanelGuid);
        if (deckArrayElement) {
            DrawStatusBarsUsa(
                560,
                760,
                deckArrayElement[6],
                GetTypeShieldMax(spriteTypeArray[deckArrayElement[0]]),
                deckArrayElement[7],
                GetTypeFuelMax(spriteTypeArray[deckArrayElement[0]]),
                deckArrayElement[8],
                GetTypeAmmoMax(spriteTypeArray[deckArrayElement[0]]),
                graphicsStatusBars
                );

            var type = spriteTypeArray[deckArrayElement[0]];
            var missileDelay = GetTypeMissileDelay(type);
        }
        if (missileDelay == 1) { // bullets
            DrawBulletChevronStatusBar('usa');
            infoPanelTextFireRate.text = "BULLET DELAY:";
        }
        else if (GetTypeAmmoMax(type) <= 0) {
            infoPanelTextFireRate.text = "UNARMED";
        }
        else {
            infoPanelTextFireRate.text = "MISSILE DELAY:";
            DrawMissileChevronStatusBar(missileDelay, 'usa');
        }

        infoPanelTextFireRate.x = 400;
        infoPanelText2.text = (type != null && type != undefined) ? GetTypeText2(type) : '';
        infoPanelText3.text = (type != null && type != undefined) ? GetTypeText3(type) : '';

        infoPanelAmmoText.text = "AMMO";
        infoPanelShieldText.text = "SHIELD";
        infoPanelFuelText.text = "FUEL";
    }

    if (spriteInfoPanelSlot != null) {
        if (ussrProfiles[spriteInfoPanelSlot]) {
            var shield = ussrProfiles[spriteInfoPanelSlot][6];
            var shieldMax = GetTypeShieldMax(ussrProfiles[spriteInfoPanelSlot][0]);
            DrawStatusBarsUssr(400, 760, shield, shieldMax, graphicsStatusBars);
        }
    }
}

function DrawDeckSprites() {
    sprite_deck.forEach(function (sd) {
        DrawDeckSprite(sd);
    });
}

function DrawDeckSprite(sd) {
    var type = spriteTypeArray[sd[0]];
    var direction = directionArray[sd[1]];
    var xIso = sd[2];
    var yIso = sd[3];
    var sprite = SetSprite(xIso + (0.5 * deckOffsetX), yIso + (0.5 * deckOffsetY), deckSpriteIsoZ, 'tileset', type + direction, isoGroup);
    sprite.anchor.set(0.5, 0.5);
    sprite.smoothed = false;
    sd[10] = sprite;
}


function DrawFoxbatIntel() {
    foxbatIntelSlot1 = game.add.bitmapText(670, 360, 'fontBlue', '', 24);
    foxbatIntelSlot2 = game.add.bitmapText(670, 260, 'fontBlue', '', 24);
    foxbatIntelSlot3 = game.add.bitmapText(670, 160, 'fontBlue', '', 24);
    foxbatIntelSlot4 = game.add.bitmapText(670, 60, 'fontBlue', '', 24);
    foxbatIntelSlot1.visible = false;
    foxbatIntelSlot2.visible = false;
    foxbatIntelSlot3.visible = false;
    foxbatIntelSlot4.visible = false;
    foxbatIntelTexts.push(foxbatIntelSlot1);
    foxbatIntelTexts.push(foxbatIntelSlot2);
    foxbatIntelTexts.push(foxbatIntelSlot3);
    foxbatIntelTexts.push(foxbatIntelSlot4);
    game.world.bringToTop(foxbatIntelTexts);
}

function DrawMainTitle() {
    mainTitleLine1Text = game.add.bitmapText(120, 25, 'fontBlue', '   harrier', 96);
    mainTitleLine2Text = game.add.bitmapText(120, 125, 'fontBlue', '   carrier', 96);
    mainTitleLine3Text = game.add.bitmapText(120, 225, 'fontRed', '', 96);
    cold_war.visible = true;
    game.time.events.add(Phaser.Timer.SECOND * 4, function () {
        mainTitleLine1Text.visible = false;
        mainTitleLine2Text.visible = false;
        mainTitleLine3Text.visible = false;
        cold_war.visible = false;
    });
}

function DrawWaveText(textOne, textTwo, textThree) {
    mainTitleLine1Text.visible = true;
    mainTitleLine1Text.text = textOne;
    game.world.bringToTop(mainTitleLine1Text);

    game.time.events.add(Phaser.Timer.SECOND * 0.7, function () {
        mainTitleLine2Text.visible = mainTitleLine1Text.visible; //if Text1 is off, so is this
        mainTitleLine2Text.text = textTwo;
        game.world.bringToTop(mainTitleLine2Text);
    });

    if (textThree != '') {
        game.time.events.add(Phaser.Timer.SECOND * 1.4, function () {
            mainTitleLine3Text.visible = mainTitleLine1Text.visible; //if Text1 is off, so is this
            mainTitleLine3Text.text = textThree;
            game.world.bringToTop(mainTitleLine3Text);
        });
    }

    game.time.events.add(Phaser.Timer.SECOND * 2, function () {
        mainTitleLine1Text.visible = false;
        mainTitleLine2Text.visible = false;
        mainTitleLine3Text.visible = false;
    });
}

function DrawRecoveryText() {
    recoveryAmmoText = game.add.bitmapText(100, 690, 'fontRed', 'ammo', 32);
    recoveryShieldText = game.add.bitmapText(150, 670, 'fontYellow', 'shield', 32);
    recoveryFuelText = game.add.bitmapText(200, 630, 'fontPurple', 'fuel', 32);
    recoveryBolterText = game.add.bitmapText(250, 600, 'fontBlue', 'bolter', 64);
    recoveryAmmoText.visible = false;
    recoveryShieldText.visible = false;
    recoveryFuelText.visible = false;
    recoveryBolterText.visible = false;
}

function DrawMissileHitText(x, y, color, text, size) {
    var missileText = game.add.bitmapText(x, y, 'font' + color, text, size); //colour must be : Red, Purple, Yellow, Black
    game.world.bringToTop(missileText);
    game.time.events.add(Phaser.Timer.SECOND * 0.5, function () {
        missileText.destroy();
    });
}

function DrawRechargeText(x, y, color, text, size) {
    var rechargeText = game.add.bitmapText(x, y, 'font' + color, text, size); //colour must be : Red, Purple, Yellow, Black
    game.world.bringToTop(rechargeText);

    var tweenRechargeText = game.add.tween(rechargeText);
    tweenRechargeText.to({ alpha: 0, y: y - 100 }, 2000 * tweenMultiplier, null, false);
    tweenRechargeText.start();
    tweenRechargeText.onComplete.add(function (sprite, tween) {
        sprite.destroy();
    });
}

function DrawHealthBars() {

    usaHealthBarDelta = SetFlatSprite(0, -100, 0, 'tileset', 'usa_health_delta', isoGroup);
    usaHealthBarDelta.scale.setTo(1, 2);
    usaHealthBar = SetFlatSprite(0, 0, 0, 'tileset', 'flag_bar_usa', isoGroup);
    usaHealthBar.scale.setTo(2, 2.2);
    usaHealthBar.sendToBack();

    ussrHealthBarDelta = SetFlatSprite(636, -100, 0, 'tileset', 'ussr_health_delta', isoGroup);
    ussrHealthBarDelta.scale.setTo(1, 2);
    ussrHealthBar = SetFlatSprite(636, 0, 0, 'tileset', 'flag_bar_ussr', isoGroup);
    ussrHealthBar.scale.setTo(2, 2.2);
    ussrHealthBar.sendToBack();
}

function DrawMissileFade(missile) {
    var tweenMissedMissile = game.add.tween(missile);
    tweenMissedMissile.to({ alpha: 0 }, 2000 * tweenMultiplier, null, false);
    tweenMissedMissile.start();
    tweenMissedMissile.onComplete.add(function (sprite, tween) {
        sprite.destroy();
    });
}

function DrawMissileDrop(missile) {
    missile.sendToBack();
    var tweenMissedMissile = game.add.tween(missile);
    tweenMissedMissile.to({ y: 500, angle: 50 }, 1500 * tweenMultiplier, null, false);
    tweenMissedMissile.start();
    tweenMissedMissile.onComplete.add(function (sprite, tween) {
        sprite.destroy();
    });
}

function DrawMoveIndicators(movingSpriteObject) {

    var x = movingSpriteObject[2];
    var y = movingSpriteObject[3];
    currentPositionIndicator = SetSprite(x + (0.5 * deckOffsetX), y + (0.5 * deckOffsetY), deckSpriteIsoZ - 8, 'tileset', 'green_square', isoGroup);
    currentPositionIndicator.anchor.set(0.5, 0.5);
    currentPositionIndicator.alpha = 0.6;

    currentPositionIndicatorBox = SetSprite(x + (0.5 * deckOffsetX), y + (0.5 * deckOffsetY), deckSpriteIsoZ + 5, 'tileset', 'green_box', isoGroup);
    currentPositionIndicatorBox.anchor.set(0.5, 0.5);
    currentPositionIndicatorBox.scale.setTo(0.9, 0.9);
    currentPositionIndicatorBox.bringToTop();


    //see: https://github.com/qiao/PathFinding.js
    var pathGrid = new PF.Grid(deckWidth, deckHeight);
    var finder = new PF.AStarFinder();

    sprite_deck.forEach(function (sd) {
        if (sd)
            if (isOnDeck(sd[9]) && sd[2] >= 0 && sd[3] >= 0)
                pathGrid.setWalkableAt(sd[2], sd[3], false);
    });

    for (var xIso = 0; xIso < deckWidth; xIso++) {
        for (var yIso = 0; yIso < deckHeight; yIso++) {

            var gridExpendable = pathGrid.clone();
            var path = finder.findPath(movingSpriteObject[2], movingSpriteObject[3], xIso, yIso, gridExpendable);
            var isConventionalFlightCatapultSquare = !isHoverFlightType(spriteTypeArray[movingSpriteObject[0]]) && isOnCatapult(xIso, yIso);

            if (path.length > 0 || isConventionalFlightCatapultSquare) {

                var sprite = SetSprite(xIso + (0.5 * deckOffsetX), yIso + (0.5 * deckOffsetY), deckSpriteIsoZ - 8, 'tileset', 'green_square', isoGroup);
                sprite.inputEnabled = true;
                sprite.input.useHandCursor = true;
                sprite.anchor.set(0.5, 0.5);
                sprite.alpha = 0.3;
                sprite.xIso = xIso;
                sprite.yIso = yIso;
                sprite.path = path;
                sprite.parentGuid = movingSpriteObject[12];
                sprite.bringToTop();

                // add takeoff arrow over catapult
                if (isConventionalFlightCatapultSquare) {
                    spriteTakeoffArrow = SetSprite(catapultCoords[0] + (0.5 * deckOffsetX) - 0.4, catapultCoords[1] + (0.5 * deckOffsetY) + 0.1, deckSpriteIsoZ + 20, 'tileset', 'vtol_arrow', isoGroup);
                    spriteTakeoffArrow.inputEnabled = true;
                    spriteTakeoffArrow.input.useHandCursor = true;
                    spriteTakeoffArrow.scale.setTo(0.8, 0.8);
                    spriteTakeoffArrow.anchor.set(-0, 0.5);
                    spriteTakeoffArrow.parentGuid = movingSpriteObject[12];

                    spriteTakeoffArrow.events.onInputDown.add(function (sprite) {
                        var spriteObject = findElement(sprite_deck, 12, sprite.parentGuid);

                        //set the moving sprites destination
                        spriteObject[4] = catapultCoords[0];
                        spriteObject[5] = catapultCoords[1];
                        spriteObject[9] = 'move_on_deck';
                        DestroyAllSelections();
                    });

                    moveIndicators.push(spriteTakeoffArrow);
                }

                // EVENT (move indicator click) 
                sprite.events.onInputDown.add(function (sprite) {
                    game.iso.unproject(game.input.activePointer.position, cursorPos);
                    graphicsStatusBars.clear();

                    if (!sprite.isoBounds.containsXY(cursorPos.x + cursorPosCorrection, cursorPos.y + cursorPosCorrection)) {
                        RedirectSpritesOnInputDownEvent(cursorPos.x + cursorPosCorrection, cursorPos.y + cursorPosCorrection);
                    }
                    else {
                        DestroyAllSelections();

                        var spriteObject = findElement(sprite_deck, 12, sprite.parentGuid);

                        if (spriteObject) {
                            //set the moving sprites destination
                            spriteObject[4] = sprite.xIso;
                            spriteObject[5] = sprite.yIso;
                            spriteObject[9] = 'move_on_deck';
                        }
                        //show projected path
                        sprite.path.forEach(function (pathStep) {
                            var spritePathChosen = SetSprite((pathStep[0]) + (0.5 * deckOffsetX), (pathStep[1]) + (0.5 * deckOffsetY), deckSpriteIsoZ - 8, 'tileset', 'blue_square', isoGroup);
                            spritePathChosen.anchor.set(0.5, 0.5);
                            //    spritePathChosen.scale.setTo(0.8, 0.8);
                            spritePathChosen.alpha = 0.3;
                            chosenPath.push(spritePathChosen);

                            game.time.events.add(Phaser.Timer.SECOND * 0.4, function () {
                                DestroyChosenPath();
                            });
                        });
                        if (sprite.path[sprite.path.length - 1]) {
                            newPositionIndicatorBox = SetSprite(sprite.path[sprite.path.length - 1][0] + (0.5 * deckOffsetX), sprite.path[sprite.path.length - 1][1] + (0.5 * deckOffsetY), deckSpriteIsoZ + 5, 'tileset', 'blue_box', isoGroup);
                            newPositionIndicatorBox.anchor.set(0.5, 0.5);
                            newPositionIndicatorBox.scale.setTo(0.9, 0.9);
                        }
                    }
                }, this);
                moveIndicators.push(sprite);
            }
        }
    }
}

//============== DESTROY ==========================

function DestroyMoveIndicators() {

    if (currentPositionIndicator)
        currentPositionIndicator.destroy();
    if (currentPositionIndicatorBox)
        currentPositionIndicatorBox.destroy();
    moveIndicators.forEach(function (moveIndicator) {
        moveIndicator.destroy();
    });
    moveIndicators = [];
}

function DestroyChosenPath() {
    if (newPositionIndicatorBox)
        newPositionIndicatorBox.destroy();
    chosenPath.forEach(function (chosenPathStep) {
        chosenPathStep.destroy();
    });
    chosenPath = [];
}

function DestroySpriteButtons() {
    spriteButtons.forEach(function (sb) {
        sb.destroy();
    });
    spriteButtons = [];
}

function DestroyUsaChevron(profileSlot) {
    if (usaProfiles[profileSlot]) {
        usaProfiles[profileSlot][9].loadTexture('tileset', 'nothing');
        usaProfiles[profileSlot][10].loadTexture('tileset', 'nothing');
    }
}

function DestroyUssrChevron(profileSlot) {
    if (ussrProfiles[profileSlot]) {
        ussrProfiles[profileSlot][9].loadTexture('tileset', 'nothing');
    }
}

function DestroySpriteDeck() {
    sprite_deck.forEach(function (sd) {
        if (sd)
            sd[10].destroy();
    });
    sprite_deck = [];
}

function DestroyUsaProfiles() {
    for (var i = 0; i < usaProfiles.length; i++) {
        if (usaProfiles[i]) {
            var returningPlane = game.add.sprite(350, 20 + (100 * (3 - i)), 'tileset', usaProfiles[i][0] + '_profile_level', usaProfileGroup);
            returningPlane.scale.setTo(-1, 1);
            var tweenReturningPlane = game.add.tween(returningPlane);
            tweenReturningPlane.to({ x: -100 }, 600 * tweenMultiplier, Phaser.Easing.Quadratic.In, false);
            tweenReturningPlane.start();
            tweenReturningPlane.onComplete.add(function (sprite, tween) {
                sprite.destroy();
            });

            usaProfiles[i][1].loadTexture('tileset', 'nothing');
        }
    }
    usaProfiles = [null, null, null, null];
}

function DestroyUssrProfiles() {
    ussrProfiles.forEach(function (profile) {
        if (profile) {
            profile[1].loadTexture('tileset', 'nothing');
            profile[1].destroy();
        }
    });
    ussrProfiles = [null, null, null, null];
}

function DestroyUsaMissiles() {

    usaMissiles.forEach(function (missile) {
        if (missile.alive) {
            missile.loadTexture('tileset', 'nothing');
            missile.destroy();
        }
    });
    usaMissiles = [];
}

function DestroyUssrMissiles() {
    ussrMissiles.forEach(function (missile) {
        if (missile.alive) {
            missile.loadTexture('tileset', 'nothing');
            missile.destroy();
        }
    });
    ussrMissiles = [];
}

function DestroyInfoPanel() {
    admiral.visible = false;

    mouthAnimation.visible = false;
    mouthAnimation.animations.stop();

    spriteInfoPanelProfile.loadTexture('tileset', 'nothing');
    spriteInfoPanelBackground.visible = false;

    spriteInfoPanelGuid = null;
    spriteInfoPanelSlot = null;
    missileChevronStatusBar.loadTexture('tileset', 'nothing');
    infoPanelTextFireRate.text = "";
    infoPanelText2.text = "";
    infoPanelText3.text = "";
    infoPanelLevelText1.text = "";
    infoPanelLevelText2.text = "";
    infoPanelLevelText3.text = "";
    infoPanelLevelText4.text = "";
    infoPanelLevelText5.text = "";
    infoPanelLevelText6.text = "";
    infoPanelAmmoText.text = "";
    infoPanelShieldText.text = "";
    infoPanelFuelText.text = "";
}

function DestroyAll() {
    DestroyUsaChevron(0);
    DestroyUsaChevron(1);
    DestroyUsaChevron(2);
    DestroyUsaChevron(3);
    DestroyUssrChevron(0);
    DestroyUssrChevron(1);
    DestroyUssrChevron(2);
    DestroyUssrChevron(3);

    DestroySpriteDeck();
    DestroyUssrProfiles();
    DestroyUsaMissiles();
    DestroyUssrMissiles();
    DestroyUsaProfiles();
}

function DestroyAllSelections() {
    if (infoPanelTitleText)
        infoPanelTitleText.visible = false;
    graphicsStatusBars.clear();
    DestroyMoveIndicators();
    DestroySpriteButtons();
    DestroyChosenPath();
    DestroyInfoPanel();
}

//=============================== UTILS ================================
function clone(obj) {
    // Handle the 3 simple types, and null or undefined
    if (null == obj || "object" != typeof obj) return obj;

    // Handle Date
    if (obj instanceof Date) {
        var copy = new Date();
        copy.setTime(obj.getTime());
        return copy;
    }

    // Handle Array
    if (obj instanceof Array) {
        var copy = [];
        for (var i = 0, len = obj.length; i < len; i++) {
            copy[i] = clone(obj[i]);
        }
        return copy;
    }

    // Handle Object
    if (obj instanceof Object) {
        var copy = {
        };
        for (var attr in obj) {
            if (obj.hasOwnProperty(attr)) copy[attr] = clone(obj[attr]);
        }
        return copy;
    }

    throw new Error("Unable to copy obj! Its type isn't supported.");
}

function guid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
          .toString(16)
          .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
      s4() + '-' + s4() + s4() + s4();
}

function findElement(arr, j, propValue) {
    for (var i = 0; i < arr.length; i++)
        if (arr[i][j] == propValue)
            return arr[i];
}

//=========== audio ==================
var audioTrack = 0;
var soundMusic = new Howl({
    src: 'assets/audio.mp4',
    loop: true,
    volume: 1
});
var soundMusic2 = new Howl({
    src: 'assets/audio2.mp4',
    loop: true,
    volume: 1
});
function MusicOn() {
    MusicOff();
    if (audioTrack == 0) {
        audioTrack = 1;
        soundMusic.play();
    }
    else {
        audioTrack = 0;
        soundMusic2.play();
    }
}
function MusicOff() {
    soundMusic.stop();
    soundMusic2.stop();
}


//================================================================================
var tiles_sea = [
[0, 0, 0, 0, 0, 0, 0, 1],
[0, 0, 0, 0, 0, 0, 0, 1],
[0, 0, 0, 0, 0, 0, 0, 1],
[0, 0, 0, 0, 0, 0, 0, 1],
[0, 0, 0, 0, 0, 0, 0, 1],
[0, 0, 0, 0, 0, 0, 0, 1],
[0, 0, 0, 0, 0, 0, 0, 1],
[0, 0, 0, 0, 0, 0, 0, 1],
[0, 0, 0, 0, 0, 0, 0, 1],
[0, 0, 0, 0, 0, 0, 0, 1],
[0, 0, 0, 0, 0, 0, 0, 1],
[0, 0, 0, 0, 0, 0, 0, 1],
[0, 0, 0, 0, 0, 0, 0, 1],
[0, 0, 0, 0, 0, 0, 0, 1],
[0, 0, 0, 0, 0, 0, 0, 1],
[0, 0, 0, 0, 0, 0, 1, 1],
[2, 2, 2, 2, 2, 2, 2, 0]
];







function AnimateWater() {
    water.forEach(function (w) {
        var gameTimeNow = game.time.now;
        w.isoZ = (10 * Math.sin((gameTimeNow - (w.isoY * 9)) * 0.0008)) + (-1 * Math.sin((gameTimeNow + (w.isoX * 30)) * 0.002)) - 130;
        w.alpha = Phaser.Math.clamp(1 + (w.isoZ * 0.6), 0.9, 1);
    });
};


function DrawSea() {

    var xOffset = 5.5;
    var yOffset = 4;

    for (var y = 0; y < tiles_sea.length; y++) {
        for (var x = 0; x < tiles_sea[y].length; x++) {

            var tileValue = tiles_sea[y][x];
            if (tileValue != 0) {

                var tileName = (tileValue == 1) ? 'white_lines_cable_purple' : 'white_lines_cable_yellow';

                var tile = SetSprite((0.5 * x) + xOffset, (0.5 * y) + yOffset, 0, 'tileset', tileName, isoGroup);
                tile.anchor.set(0.5, 0.5);
                tile.smoothed = false;
                water.push(tile);
                console.log('k')
            }
        }
    }
};