{"frames": {
"insignia_1":
{
	"frame": {"x":480,"y": 130,"w":96,"h":102},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":96,"h":130},
	"sourceSize": {"w":96,"h":130}
},
"insignia_2":
{
	"frame": {"x":384,"y": 130,"w":96,"h":102},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":96,"h":130},
	"sourceSize": {"w":96,"h":130}
},
"insignia_3":
{
	"frame": {"x":288,"y": 130,"w":96,"h":102},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":96,"h":130},
	"sourceSize": {"w":96,"h":130}
},
"insignia_4":
{
	"frame": {"x":192,"y": 130,"w":96,"h":102},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":96,"h":130},
	"sourceSize": {"w":96,"h":130}
},
"insignia_5":
{
	"frame": {"x":96,"y": 130,"w":96,"h":102},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":96,"h":130},
	"sourceSize": {"w":96,"h":130}
},
"insignia_6":
{
	"frame": {"x":0,"y": 130,"w":96,"h":102},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":96,"h":130},
	"sourceSize": {"w":96,"h":130}
},
"insignia_7":
{
	"frame": {"x":480,"y": 0,"w":96,"h":130},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":96,"h":130},
	"sourceSize": {"w":96,"h":130}
},
"insignia_8":
{
	"frame": {"x":384,"y": 0,"w":96,"h":130},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":96,"h":130},
	"sourceSize": {"w":96,"h":130}
},
"insignia_9":
{
	"frame": {"x":288,"y": 0,"w":96,"h":130},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":96,"h":130},
	"sourceSize": {"w":96,"h":130}
},
"insignia_10":
{
	"frame": {"x":192,"y": 0,"w":96,"h":130},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":96,"h":130},
	"sourceSize": {"w":96,"h":130}
},
"insignia_11":
{
	"frame": {"x":96,"y": 0,"w":96,"h":130},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":96,"h":130},
	"sourceSize": {"w":96,"h":130}
},
"insignia_12":
{
	"frame": {"x":0,"y": 0,"w":96,"h":130},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":96,"h":130},
	"sourceSize": {"w":96,"h":130}
},
"insignia_13":
{
	"frame": {"x":0,"y": 0,"w":96,"h":130},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":96,"h":130},
	"sourceSize": {"w":96,"h":130}
},
"insignia_14":
{
	"frame": {"x":0,"y": 0,"w":96,"h":130},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":96,"h":130},
	"sourceSize": {"w":96,"h":130}
},
"insignia_15":
{
	"frame": {"x":0,"y": 0,"w":96,"h":130},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":96,"h":130},
	"sourceSize": {"w":96,"h":130}
},
"insignia_16":
{
	"frame": {"x":0,"y": 0,"w":96,"h":130},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":96,"h":130},
	"sourceSize": {"w":96,"h":130}
}
},
"meta": {
	"app": "http://www.codeandweb.com/texturepacker ",
	"version": "1.0",
	"image": "tileset.png",
	"format": "RGBA5551",
	"size": {"w":600,"h":800},
	"scale": "1",
	"smartupdate": "$TexturePacker:SmartUpdate:682f909cd66baf330156be59a66e712b:3003e449284409367ce481c13a81bcfb:ace0e5e82375743ccde85e973fd84fb5$"
}
}