{"frames": {
"nothing":
{
	"frame": {"x":0,"y": 0,"w":1,"h":1},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":1,"h":1},
	"sourceSize": {"w":1,"h":1}
},
"burger":
{
	"frame": {"x":0,"y":224,"w":128,"h":112},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":128,"h":112},
	"sourceSize": {"w":128,"h":112}
},
"tower":
{
	"frame": {"x":64,"y":240,"w":64,"h":80},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":80},
	"sourceSize": {"w":64,"h":80}
},
"deck_block":
{
	"frame": {"x":128,"y":0,"w":64,"h":80},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":80},
	"sourceSize": {"w":64,"h":80}
},
"deck_thin":
{
	"frame": {"x":0,"y":448,"w":64,"h":80},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":80},
	"sourceSize": {"w":64,"h":64}
},
"lift_block":
{
	"frame": {"x":128,"y":80,"w":64,"h":90},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":90},
	"sourceSize": {"w":64,"h":90}
},
"lift_back":
{
	"frame": {"x":128,"y":80,"w":64,"h":32},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":32},
	"sourceSize": {"w":64,"h":32}
},
"lift_hatch_west":
{
	"frame": {"x":128,"y":416,"w":64,"h":80},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":80},
	"sourceSize": {"w":64,"h":80}
},
"white_lines":
{
	"frame": {"x":0,"y":544,"w":64,"h":80},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":80},
	"sourceSize": {"w":64,"h":80}
},
"mask_1":
{
	"frame": {"x":656,"y":720,"w":64,"h":32},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":32},
	"sourceSize": {"w":64,"h":32}
},
"mask_2":
{
	"frame": {"x":592,"y":672,"w":64,"h":32},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":32},
	"sourceSize": {"w":64,"h":32}
},
"mask_3":
{
	"frame": {"x":656,"y":672,"w":64,"h":32},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":32},
	"sourceSize": {"w":64,"h":32}
},
"mask_4":
{
	"frame": {"x":592,"y":720,"w":64,"h":32},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":32},
	"sourceSize": {"w":64,"h":32}
},
"catapult_west":
{
	"frame": {"x":0,"y":640,"w":64,"h":80},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":80},
	"sourceSize": {"w":64,"h":80}
},
"catapult_east":
{
	"frame": {"x":64,"y":640,"w":64,"h":80},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":80},
	"sourceSize": {"w":64,"h":80}
},
"catapult_west_brown":
{
	"frame": {"x":128,"y":640,"w":64,"h":80},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":80},
	"sourceSize": {"w":64,"h":80}
},
"catapult_east_brown":
{
	"frame": {"x":192,"y":640,"w":64,"h":80},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":80},
	"sourceSize": {"w":64,"h":80}
},
"plane":
{
	"frame": {"x":134,"y":93,"w":64,"h":37},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":37},
	"sourceSize": {"w":64,"h":37}
},
"lift_hatch_1":
{
	"frame": {"x":128,"y":240,"w":64,"h":80},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":80},
	"sourceSize": {"w":64,"h":80}
},
"lift_hatch_2":
{
	"frame": {"x":128,"y":320,"w":64,"h":80},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":80},
	"sourceSize": {"w":64,"h":80}
},
"harrier":
{
	"frame": {"x":134,"y":54,"w":64,"h":37},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":37},
	"sourceSize": {"w":64,"h":37}
},
"se_corner":
{
	"frame": {"x":64,"y":80,"w":64,"h":80},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":80},
	"sourceSize": {"w":64,"h":80}
},
"ne_corner":
{
	"frame": {"x":65,"y":0,"w":64,"h":80},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":80},
	"sourceSize": {"w":64,"h":80}
},
"sw_corner":
{
	"frame": {"x":0,"y":240,"w":64,"h":80},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":80},
	"sourceSize": {"w":64,"h":80}
},
"nw_corner":
{
	"frame": {"x":0,"y":0,"w":64,"h":80},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":80},
	"sourceSize": {"w":64,"h":80}
},
"n_wall":
{
	"frame": {"x":0,"y":160,"w":64,"h":80},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":80},
	"sourceSize": {"w":64,"h":80}
},
"s_wall":
{
	"frame": {"x":0,"y":320,"w":64,"h":80},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":80},
	"sourceSize": {"w":64,"h":80}
},
"w_wall":
{
	"frame": {"x":0,"y":80,"w":63,"h":80},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":63,"h":80},
	"sourceSize": {"w":63,"h":80}
},
"e_wall":
{
	"frame": {"x":65,"y":161,"w":64,"h":80},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":80},
	"sourceSize": {"w":64,"h":80}
},
"floor":
{
	"frame": {"x":0,"y":400,"w":64,"h":80},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":80},
	"sourceSize": {"w":64,"h":80}
},
"water":
{
	"frame": {"x":64,"y":447,"w":64,"h":42},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":42},
	"sourceSize": {"w":64,"h":42}
},
"white_water":
{
	"frame": {"x":64,"y":496,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"half_water":
{
	"frame": {"x":64,"y":544,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"button":
{
	"frame": {"x":144,"y":496,"w":64,"h":42},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":42},
	"sourceSize": {"w":64,"h":42}
},
"empty":
{
	"frame": {"x":2,"y":2,"w":64,"h":75},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":75},
	"sourceSize": {"w":64,"h":75}
},
"harrier_n":
{
	"frame": {"x":208,"y":0,"w":64,"h":48},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":48},
	"sourceSize": {"w":64,"h":48}
},
"harrier_e":
{
	"frame": {"x":208,"y":48,"w":64,"h":48},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":48},
	"sourceSize": {"w":64,"h":48}
},
"harrier_s":
{
	"frame": {"x":208,"y":96,"w":64,"h":48},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":48},
	"sourceSize": {"w":64,"h":48}
},
"harrier_w":
{
	"frame": {"x":208,"y":144,"w":64,"h":48},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":48},
	"sourceSize": {"w":64,"h":48}
},
"tomcat_n":
{
	"frame": {"x":272,"y":0,"w":64,"h":48},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":48},
	"sourceSize": {"w":64,"h":48}
},
"tomcat_e":
{
	"frame": {"x":272,"y":48,"w":64,"h":48},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":48},
	"sourceSize": {"w":64,"h":48}
},
"tomcat_s":
{
	"frame": {"x":272,"y":96,"w":64,"h":48},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":48},
	"sourceSize": {"w":64,"h":48}
},
"tomcat_w":
{
	"frame": {"x":272,"y":144,"w":64,"h":48},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":48},
	"sourceSize": {"w":64,"h":48}
},
"hawkeye_n":
{
	"frame": {"x":336,"y":0,"w":64,"h":48},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":48},
	"sourceSize": {"w":64,"h":48}
},
"hawkeye_e":
{
	"frame": {"x":336,"y":48,"w":64,"h":48},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":48},
	"sourceSize": {"w":64,"h":48}
},
"hawkeye_s":
{
	"frame": {"x":336,"y":96,"w":64,"h":48},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":48},
	"sourceSize": {"w":64,"h":48}
},
"hawkeye_w":
{
	"frame": {"x":336,"y":144,"w":64,"h":48},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":48},
	"sourceSize": {"w":64,"h":48}
},
"huey_n":
{
	"frame": {"x":208,"y":528,"w":64,"h":48},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":48},
	"sourceSize": {"w":64,"h":48}
},
"huey_e":
{
	"frame": {"x":208,"y":576,"w":64,"h":48},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":48},
	"sourceSize": {"w":64,"h":48}
},
"huey_s":
{
	"frame": {"x":208,"y":624,"w":64,"h":48},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":48},
	"sourceSize": {"w":64,"h":48}
},
"huey_w":
{
	"frame": {"x":208,"y":672,"w":64,"h":48},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":48},
	"sourceSize": {"w":64,"h":48}
},
"u2_n":
{
	"frame": {"x":144,"y":336,"w":64,"h":48},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":48},
	"sourceSize": {"w":64,"h":48}
},
"u2_e":
{
	"frame": {"x":144,"y":384,"w":64,"h":48},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":48},
	"sourceSize": {"w":64,"h":48}
},
"u2_s":
{
	"frame": {"x":144,"y":432,"w":64,"h":48},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":48},
	"sourceSize": {"w":64,"h":48}
},
"u2_w":
{
	"frame": {"x":144,"y":480,"w":64,"h":48},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":48},
	"sourceSize": {"w":64,"h":48}
},
"seaking_n":
{
	"frame": {"x":208,"y":336,"w":64,"h":48},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":48},
	"sourceSize": {"w":64,"h":48}
},
"seaking_e":
{
	"frame": {"x":208,"y":384,"w":64,"h":48},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":48},
	"sourceSize": {"w":64,"h":48}
},
"seaking_s":
{
	"frame": {"x":208,"y":432,"w":64,"h":48},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":48},
	"sourceSize": {"w":64,"h":48}
},
"seaking_w":
{
	"frame": {"x":208,"y":480,"w":64,"h":48},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":48},
	"sourceSize": {"w":64,"h":48}
},
"warthog_n":
{
	"frame": {"x":801,"y":0,"w":62,"h":48},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":62,"h":48},
	"sourceSize": {"w":62,"h":48}
},
"warthog_e":
{
	"frame": {"x":801,"y":48,"w":62,"h":48},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":62,"h":48},
	"sourceSize": {"w":62,"h":48}
},
"warthog_s":
{
	"frame": {"x":801,"y":96,"w":62,"h":48},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":62,"h":48},
	"sourceSize": {"w":62,"h":48}
},
"warthog_w":
{
	"frame": {"x":801,"y":144,"w":62,"h":48},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":62,"h":48},
	"sourceSize": {"w":62,"h":48}
},
"foxbat_defect_n":
{
	"frame": {"x":864,"y":0,"w":62,"h":48},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":62,"h":48},
	"sourceSize": {"w":62,"h":48}
},
"foxbat_defect_e":
{
	"frame": {"x":864,"y":48,"w":62,"h":48},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":62,"h":48},
	"sourceSize": {"w":62,"h":48}
},
"foxbat_defect_s":
{
	"frame": {"x":864,"y":96,"w":62,"h":48},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":62,"h":48},
	"sourceSize": {"w":62,"h":48}
},
"foxbat_defect_w":
{
	"frame": {"x":864,"y":144,"w":62,"h":48},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":62,"h":48},
	"sourceSize": {"w":62,"h":48}
},
"blue_box":
{
	"frame": {"x":0,"y": 0,"w":128,"h":96},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":128,"h":96},
	"sourceSize": {"w":128,"h":96}
},
"green_box":
{
	"frame": {"x":0,"y": 112,"w":128,"h":95},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":128,"h":95},
	"sourceSize": {"w":128,"h":95}
},
"blue_square":
{
	"frame": {"x":800,"y": 592,"w":128,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":128,"h":64},
	"sourceSize": {"w":128,"h":64}
},
"green_square":
{
	"frame": {"x":800,"y": 672,"w":128,"h":63},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":128,"h":63},
	"sourceSize": {"w":128,"h":63}
},
"purple_square":
{
	"frame": {"x":592,"y": 640,"w":64,"h":80},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":80},
	"sourceSize": {"w":64,"h":80}
},
"yellow_square":
{
	"frame": {"x":720,"y": 640,"w":64,"h":80},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":80},
	"sourceSize": {"w":64,"h":80}
},
"red_square":
{
	"frame": {"x":656,"y": 640,"w":64,"h":80},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":80},
	"sourceSize": {"w":64,"h":80}
},
"harrier_profile_level":
{
	"frame": {"x":208,"y": 256,"w":128,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":128,"h":64},
	"sourceSize": {"w":128,"h":64}
},
"harrier_profile_up":
{
	"frame": {"x":416,"y": 0,"w":128,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":128,"h":64},
	"sourceSize": {"w":128,"h":64}
},
"harrier_profile_down":
{
	"frame": {"x":416,"y": 64,"w":128,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":128,"h":64},
	"sourceSize": {"w":128,"h":64}
},
"tomcat_profile_level":
{
	"frame": {"x":208,"y": 192,"w":128,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":128,"h":64},
	"sourceSize": {"w":128,"h":64}
},
"tomcat_profile_up":
{
	"frame": {"x":320,"y": 656,"w":128,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":128,"h":64},
	"sourceSize": {"w":128,"h":64}
},
"tomcat_profile_down":
{
	"frame": {"x":320,"y": 592,"w":128,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":128,"h":64},
	"sourceSize": {"w":128,"h":64}
},
"huey_profile_level":
{
	"frame": {"x":320,"y": 720,"w":128,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":128,"h":64},
	"sourceSize": {"w":128,"h":64}
},
"huey_profile_up":
{
	"frame": {"x":320,"y": 720,"w":128,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":128,"h":64},
	"sourceSize": {"w":128,"h":64}
},
"huey_profile_down":
{
	"frame": {"x":320,"y": 720,"w":128,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":128,"h":64},
	"sourceSize": {"w":128,"h":64}
},
"u2_profile_level":
{
	"frame": {"x":64,"y": 528,"w":128,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":128,"h":64},
	"sourceSize": {"w":128,"h":64}
},
"u2_profile_up":
{
	"frame": {"x":64,"y": 592,"w":128,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":128,"h":64},
	"sourceSize": {"w":128,"h":64}
},
"u2_profile_down":
{
	"frame": {"x":64,"y": 656,"w":128,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":128,"h":64},
	"sourceSize": {"w":128,"h":64}
},
"seaking_profile_level":
{
	"frame": {"x":592,"y": 16,"w":128,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":128,"h":64},
	"sourceSize": {"w":128,"h":64}
},
"seaking_profile_up":
{
	"frame": {"x":592,"y": 80,"w":128,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":128,"h":64},
	"sourceSize": {"w":128,"h":64}
},
"seaking_profile_down":
{
	"frame": {"x":592,"y": 144,"w":128,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":128,"h":64},
	"sourceSize": {"w":128,"h":64}
},
"hawkeye_profile_level":
{
	"frame": {"x":336,"y": 192,"w":128,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0, "w":128,"h":64},
	"sourceSize": {"w":128,"h":64}
},
"hawkeye_profile_up":
{
	"frame": {"x":464,"y": 192,"w":128,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0, "w":128,"h":64},
	"sourceSize": {"w":128,"h":64}
},
"hawkeye_profile_down":
{
	"frame": {"x":464,"y": 128,"w":128,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0, "w":128,"h":64},
	"sourceSize": {"w":128,"h":64}
},
"warthog_profile_level":
{
	"frame": {"x":816,"y": 192,"w":128,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0, "w":128,"h":64},
	"sourceSize": {"w":128,"h":64}
},
"warthog_profile_up":
{
	"frame": {"x":816,"y": 256,"w":128,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0, "w":128,"h":64},
	"sourceSize": {"w":128,"h":64}
},
"warthog_profile_down":
{
	"frame": {"x":816,"y": 320,"w":128,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0, "w":128,"h":64},
	"sourceSize": {"w":128,"h":64}
},
"missile_profile_1":
{
	"frame": {"x":272,"y": 544,"w":80,"h":32},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":80,"h":32},
	"sourceSize": {"w":80,"h":32}
},
"missile_profile_2":
{
	"frame": {"x":352,"y": 544,"w":80,"h":32},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":80,"h":32},
	"sourceSize": {"w":80,"h":32}
},
"mig15_profile_up":
{
	"frame": {"x":592,"y": 512,"w":128,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":128,"h":64},
	"sourceSize": {"w":128,"h":64}
},
"mig15_profile_down":
{
	"frame": {"x":592,"y": 448,"w":128,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":128,"h":64},
	"sourceSize": {"w":128,"h":64}
},
"mig15_profile_level":
{
	"frame": {"x":592,"y": 576,"w":128,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":128,"h":64},
	"sourceSize": {"w":128,"h":64}
},
"fulcrum_profile_level":
{
	"frame": {"x":0,"y": 416,"w":128,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":128,"h":64},
	"sourceSize": {"w":128,"h":64}
},
"fulcrum_profile_up":
{
	"frame": {"x":0,"y": 352,"w":128,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":128,"h":64},
	"sourceSize": {"w":128,"h":64}
},
"fulcrum_profile_down":
{
	"frame": {"x":464,"y": 544,"w":128,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":128,"h":64},
	"sourceSize": {"w":128,"h":64}
},
"bear_profile_level":
{	
	"frame": {"x":352,"y": 256,"w":192,"h":80},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":192,"h":80},
	"sourceSize": {"w":192,"h":80}
},
"bear_profile_up":
{	
	"frame": {"x":352,"y": 336,"w":192,"h":80},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":192,"h":80},
	"sourceSize": {"w":192,"h":80}
},
"bear_profile_down":
{	
	"frame": {"x":352,"y": 416,"w":192,"h":80},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":192,"h":80},
	"sourceSize": {"w":192,"h":80}
},
"mig21_profile_up":
{
	"frame": {"x":464,"y": 608,"w":128,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":128,"h":64},
	"sourceSize": {"w":128,"h":64}
},
"mig21_profile_level":
{
	"frame": {"x":464,"y": 672,"w":128,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":128,"h":64},
	"sourceSize": {"w":128,"h":64}
},
"mig21_profile_down":
{
	"frame": {"x":464,"y": 736,"w":128,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":128,"h":64},
	"sourceSize": {"w":128,"h":64}
},
"foxbat_profile_level":
{
	"frame": {"x":816,"y": 384 ,"w":128,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":128,"h":64},
	"sourceSize": {"w":128,"h":64}
},
"foxbat_profile_up":
{
	"frame": {"x":816,"y": 448,"w":128,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":128,"h":64},
	"sourceSize": {"w":128,"h":64}
},
"foxbat_profile_down":
{
	"frame": {"x":816,"y": 512,"w":128,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":128,"h":64},
	"sourceSize": {"w":128,"h":64}
},
"foxbat_defecting_profile_level":
{
	"frame": {"x":816,"y": 384 ,"w":126,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":126,"h":64},
	"sourceSize": {"w":126,"h":64}
},
"foxbat_defecting_profile_up":
{
	"frame": {"x":816,"y": 448,"w":126,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":126,"h":64},
	"sourceSize": {"w":126,"h":64}
},
"foxbat_defecting_profile_down":
{
	"frame": {"x":816,"y": 512,"w":126,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":126,"h":64},
	"sourceSize": {"w":126,"h":64}
},
"foxbat_defect_profile_level":
{
	"frame": {"x":608,"y": 208 ,"w":128,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":128,"h":64},
	"sourceSize": {"w":128,"h":64}
},
"foxbat_defect_profile_up":
{
	"frame": {"x":608,"y": 272,"w":128,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":128,"h":64},
	"sourceSize": {"w":128,"h":64}
},
"foxbat_defect_profile_down":
{
	"frame": {"x":608,"y": 336,"w":128,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":128,"h":64},
	"sourceSize": {"w":128,"h":64}
},
"corsair_s":
{
	"frame": {"x":272,"y":736,"w":64,"h":48},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":48},
	"sourceSize": {"w":64,"h":48}
},
"prowler_s":
{
	"frame": {"x":336,"y":736,"w":64,"h":48},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":48},
	"sourceSize": {"w":64,"h":48}
},
"lightning_s":
{
	"frame": {"x":400,"y":736,"w":64,"h":48},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":48},
	"sourceSize": {"w":64,"h":48}
},
"ussr_missile_1":
{
	"frame": {"x":352,"y":544,"w":80,"h":32},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":80,"h":32},
	"sourceSize": {"w":80,"h":32}
},
"usa_missile_1":
{
	"frame": {"x":272,"y":544,"w":80,"h":32},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":80,"h":32},
	"sourceSize": {"w":80,"h":32}
},
"explosion":
{
	"frame": {"x":736,"y":720,"w":32,"h":32},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":32,"h":32},
	"sourceSize": {"w":32,"h":32}
},
"radar_jammer":
{
	"frame": {"x":736,"y":752,"w":16,"h":32},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":16,"h":32},
	"sourceSize": {"w":16,"h":32}
},
"chevron_bullet_0":
{
	"frame": {"x":816,"y":768,"w":64,"h":15},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":15},
	"sourceSize": {"w":64,"h":15}
},
"chevron_bullet_1":
{
	"frame": {"x":816,"y":784,"w":64,"h":15},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":15},
	"sourceSize": {"w":64,"h":15}
},
"chevron_1_1_blue":
{
	"frame": {"x":880,"y":784,"w":16,"h":15},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":16,"h":15},
	"sourceSize": {"w":16,"h":15}
},
"chevron_2_2_blue":
{
	"frame": {"x":880,"y":784,"w":32,"h":15},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":32,"h":15},
	"sourceSize": {"w":32,"h":15}
},
"chevron_3_3_blue":
{
	"frame": {"x":880,"y":784,"w":48,"h":15},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":48,"h":15},
	"sourceSize": {"w":48,"h":15}
},
"chevron_5_5_blue":
{
	"frame": {"x":880,"y":784,"w":80,"h":15},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":80,"h":15},
	"sourceSize": {"w":80,"h":15}
},
"chevron_0_5":
{
	"frame": {"x":720,"y":16,"w":80,"h":15},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":80,"h":15},
	"sourceSize": {"w":80,"h":15}
},
"chevron_1_5":
{
	"frame": {"x":720,"y":32,"w":80,"h":15},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":80,"h":15},
	"sourceSize": {"w":80,"h":15}
},
"chevron_2_5":
{
	"frame": {"x":720,"y":48,"w":80,"h":15},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":80,"h":15},
	"sourceSize": {"w":80,"h":15}
},
"chevron_3_5":
{
	"frame": {"x":720,"y":64,"w":80,"h":15},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":80,"h":15},
	"sourceSize": {"w":80,"h":15}
},
"chevron_4_5":
{
	"frame": {"x":720,"y":80 ,"w":80,"h":15},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":80,"h":15},
	"sourceSize": {"w":80,"h":15}
},
"chevron_5_5":
{
	"frame": {"x":720,"y":96,"w":80,"h":15},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":80,"h":15},
	"sourceSize": {"w":80,"h":15}
},
"chevron_0_4":
{
	"frame": {"x":720,"y":16,"w":64,"h":15},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":15},
	"sourceSize": {"w":64,"h":15}
},
"chevron_1_4":
{
	"frame": {"x":720,"y":32,"w":64,"h":15},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":15},
	"sourceSize": {"w":64,"h":15}
},
"chevron_2_4":
{
	"frame": {"x":720,"y":48,"w":64,"h":15},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":15},
	"sourceSize": {"w":64,"h":15}
},
"chevron_3_4":
{
	"frame": {"x":720,"y":64,"w":64,"h":15},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":15},
	"sourceSize": {"w":64,"h":15}
},
"chevron_4_4":
{
	"frame": {"x":720,"y":80 ,"w":64,"h":15},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":15},
	"sourceSize": {"w":64,"h":15}
},
"chevron_0_3":
{
	"frame": {"x":720,"y":16,"w":48,"h":15},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":48,"h":15},
	"sourceSize": {"w":48,"h":15}
},
"chevron_1_3":
{
	"frame": {"x":720,"y":32,"w":48,"h":15},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":48,"h":15},
	"sourceSize": {"w":48,"h":15}
},
"chevron_2_3":
{
	"frame": {"x":720,"y":48,"w":48,"h":15},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":48,"h":15},
	"sourceSize": {"w":48,"h":15}
},
"chevron_3_3":
{
	"frame": {"x":720,"y":64,"w":48,"h":15},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":48,"h":15},
	"sourceSize": {"w":48,"h":15}
},
"chevron_0_2":
{
	"frame": {"x":720,"y":16,"w":32,"h":15},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":32,"h":15},
	"sourceSize": {"w":32,"h":15}
},
"chevron_1_2":
{
	"frame": {"x":720,"y":32,"w":32,"h":15},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":32,"h":15},
	"sourceSize": {"w":32,"h":15}
},
"chevron_2_2":
{
	"frame": {"x":720,"y":48,"w":32,"h":15},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":32,"h":15},
	"sourceSize": {"w":32,"h":15}
},
"chevron_0_1":
{
	"frame": {"x":720,"y":16,"w":16,"h":15},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":16,"h":15},
	"sourceSize": {"w":16,"h":15}
},
"chevron_1_1":
{
	"frame": {"x":720,"y":32,"w":16,"h":15},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":16,"h":15},
	"sourceSize": {"w":16,"h":15}
},
"info_panel_background":
{
	"frame": {"x":768,"y":752,"w":32,"h":32},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":32,"h":32},
	"sourceSize": {"w":32,"h":32}
},
"touch_icon_left":
{
	"frame": {"x":736,"y":640,"w":48,"h":32},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":48,"h":32},
	"sourceSize": {"w":48,"h":32}
},
"touch_icon_bottom":
{
	"frame": {"x":768,"y":672,"w":32,"h":48},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":32,"h":48},
	"sourceSize": {"w":32,"h":48}
},
"wait_icon":
{
	"frame": {"x":736,"y":672,"w":32,"h":48},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":32,"h":48},
	"sourceSize": {"w":32,"h":48}
},
"vtol_arrow":
{
	"frame": {"x":720,"y":544,"w":80,"h":96},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":80,"h":96},
	"sourceSize": {"w":80,"h":96}
},
"flag_bar_usa":
{
	"frame": {"x":736,"y":128,"w":32,"h":200},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":32,"h":200},
	"sourceSize": {"w":32,"h":200}
},
"flag_bar_ussr":
{
	"frame": {"x":768,"y":128,"w":32,"h":200},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":32,"h":200},
	"sourceSize": {"w":32,"h":200}
},
"usa_health_delta":
{
	"frame": {"x":736,"y":127,"w":32,"h":1},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":32,"h":1},
	"sourceSize": {"w":32,"h":1}
},
"ussr_health_delta":
{
	"frame": {"x":768,"y":127,"w":32,"h":1},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":32,"h":1},
	"sourceSize": {"w":32,"h":1}
},
"sos":
{
	"frame": {"x":880,"y": 768,"w":64,"h":14},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":14},
	"sourceSize": {"w":64,"h":14}
},
"ffwd_1_off":
{
	"frame": {"x":0,"y": 496,"w":64,"h":48},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":48},
	"sourceSize": {"w":64,"h":48}
},
"ffwd_1_on":
{
	"frame": {"x":0,"y": 544,"w":64,"h":48},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":48},
	"sourceSize": {"w":64,"h":48}
},
"ffwd_2_off":
{
	"frame": {"x":0,"y": 592,"w":64,"h":48},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":48},
	"sourceSize": {"w":64,"h":48}
},
"ffwd_2_on":
{
	"frame": {"x":0,"y": 640,"w":64,"h":48},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":48},
	"sourceSize": {"w":64,"h":48}
},
"ffwd_3_off":
{
	"frame": {"x":0,"y": 688,"w":64,"h":48},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":48},
	"sourceSize": {"w":64,"h":48}
},
"ffwd_3_on":
{
	"frame": {"x":0,"y": 736,"w":64,"h":48},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":48},
	"sourceSize": {"w":64,"h":48}
}
},
"meta": {
	"app": "http://www.codeandweb.com/texturepacker ",
	"version": "1.0",
	"image": "tileset.png",
	"format": "RGBA5551",
	"size": {"w":600,"h":800},
	"scale": "1",
	"smartupdate": "$TexturePacker:SmartUpdate:682f909cd66baf330156be59a66e712b:3003e449284409367ce481c13a81bcfb:ace0e5e82375743ccde85e973fd84fb5$"
}
}